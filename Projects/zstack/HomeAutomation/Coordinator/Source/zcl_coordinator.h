/**************************************************************************************************
Filename:       zcl_coordinator.h
Revised:        $Date: 2009-12-29 18:31:22 -0800 (Tue, 29 Dec 2009) $
Revision:       $Revision: 21416 $


Description:    This file contains the Zigbee Cluster Library Home
				Automation Sample Application.


Copyright 2006-2007 Texas Instruments Incorporated. All rights reserved.

IMPORTANT: Your use of this Software is limited to those specific rights
granted under the terms of a software license agreement between the user
who downloaded the software, his/her employer (which must be your employer)
and Texas Instruments Incorporated (the "License").  You may not use this
Software unless you agree to abide by the terms of the License. The License
limits your use, and you acknowledge, that the Software may not be modified,
copied or distributed unless embedded on a Texas Instruments microcontroller
or used solely and exclusively in conjunction with a Texas Instruments radio
frequency transceiver, which is integrated into your product.  Other than for
the foregoing purpose, you may not use, reproduce, copy, prepare derivative
works of, modify, distribute, perform, display or sell this Software and/or
its documentation for any purpose.

YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, 
INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE, 
NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

Should you have any questions regarding your right to use this Software,
contact Texas Instruments Incorporated at www.TI.com. 
**************************************************************************************************/

#ifndef ZCL_COORDINATOR_H
#define ZCL_COORDINATOR_H

#ifdef __cplusplus
extern "C"
{
#endif

	/*********************************************************************
* INCLUDES
*/
#include "zcl.h"

	/*********************************************************************
* CONSTANTS
*/
#define COORDINATOR_ENDPOINT           1

#define COORDINATOR_MAX_ATTRIBUTES     11

#define LIGHT_OFF                       0x00
#define LIGHT_ON                        0x01

	// Events for the sample app
#define COORDINATOR_IDENTIFY_TIMEOUT_EVT         0x0001

	/*********************************************************************
* MACROS
*/
	/*********************************************************************
* TYPEDEFS
*/

	/*********************************************************************
* VARIABLES
*/
	extern SimpleDescriptionFormat_t zclCoordinator_SimpleDesc;

	extern CONST zclAttrRec_t zclCoordinator_Attrs[];

	extern uint8  zclCoordinator_OnOff;

	extern uint16 zclCoordinator_IdentifyTime;

	typedef struct{
		uint8	cmdid;	 	
		uint16 rpt_interval;
		uint16 attr_id;
		uint16 cluster_id;
	}APP_CFG_RPT;

	typedef struct{
		uint8 cmdid;
	}ONOFF_CMD;

	typedef struct{
		uint8 dim_lvl;
	}LVL_CTRL_CMD;

	typedef struct
	{
		uint16	dstAddr;     // Sender's address
		uint8   endPoint;    // destination endpoint
		uint16  clusterId;   // Cluster ID
		uint8	cmdid;		 // command to be sent
		uint8	msglen;	
	} AppIncomingMsg_t;

#define APP_CMD_ID 	0x0

	/*********************************************************************
* FUNCTIONS
*/

	/*
* Initialization for the task
*/
	extern void zclCoordinator_Init( byte task_id );

	/*
*  Event Process for the task
*/
	extern UINT16 zclCoordinator_event_loop( byte task_id, UINT16 events );


	/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* ZCL_SAMPLEAPP_H */
