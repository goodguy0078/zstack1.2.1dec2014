
#if defined (HAL_DATA_LOGGER_CHINA)   
#include "onboard.h"
#include "HTC_Sensor.h"

uint8  U8T_data_H,U8T_data_L,U8RH_data_H,U8RH_data_L,U8checkdata;
uint8  U8T_data_H_temp,U8T_data_L_temp,U8RH_data_H_temp,U8RH_data_L_temp,U8checkdata_temp;
uint8  U8FLAG,k;
uint8  U8count,U8temp;
uint8  U8comdata;
uint8  count, count_r=0;
uint16 U16temp1,U16temp2;

void Delay_10us(void)
{
	Onboard_wait(10);
}

void Delay(uint16 v)
{
	Onboard_wait(v * 1000);
}

/*********************************************************************
//Code specific to Aosong sensors
*********************************************************************/
void COM(void)
{

	uint8 i;
	for(i=0;i<8;i++)	   
	{
		
		U8FLAG=2;

		while((!P2_0)&&U8FLAG++);
		Delay_10us();
		Delay_10us();
		Delay_10us();
		U8temp=0;
		if(P2_0)U8temp=1;
		U8FLAG=2;
		while((P2_0)&&U8FLAG++);
		

		if(U8FLAG==1)break;

		
		U8comdata<<=1;
		U8comdata|=U8temp;        //0
	}//rof

}

//--------------------------------
//-----Sub-program for reading %RH ------------
//--------------------------------
//----All the variable bellow is global variable--------
//----Temperature's high 8bit== U8T_data_H------
//----Temperature's low 8bit== U8T_data_L------
//----Humidity's high 8bit== U8RH_data_H-----
//----Humidity's low 8bit== U8RH_data_L-----
//----Check-sum 8bit == U8checkdata-----
//--------------------------------

void RH(void)
{

	P2_0=0;
	Delay(5);
	P2_0=1;
	Delay_10us();
	Delay_10us();
	Delay_10us();
	Delay_10us();
	P2_0=1;	  
	if(!P2_0)		  
	{
		U8FLAG=2;
		while((!P2_0)&&U8FLAG++);
		U8FLAG=2;
		while((P2_0)&&U8FLAG++);
		COM();
		U8RH_data_H_temp=U8comdata;
		COM();
		U8RH_data_L_temp=U8comdata;
		COM();
		U8T_data_H_temp=U8comdata;
		COM();
		U8T_data_L_temp=U8comdata;
		COM();
		U8checkdata_temp=U8comdata;
		P2_0=1;

		U8temp=(U8T_data_H_temp+U8T_data_L_temp+U8RH_data_H_temp+U8RH_data_L_temp);
		if(U8temp==U8checkdata_temp)
		{
			U8RH_data_H=U8RH_data_H_temp;
			U8RH_data_L=U8RH_data_L_temp;
			U8T_data_H=U8T_data_H_temp;
			U8T_data_L=U8T_data_L_temp;
			U8checkdata=U8checkdata_temp;
			short int val;
			if(U8T_data_H & 0x80)
			{
				// -ive no, take 2's complement of data[0]
				val = (short int)U8T_data_L;
				val = ~val + 1;
			} 
			else 
			{
				val = (short int) U8T_data_L + (short int)(U8T_data_H <<8);
			}
			U8T_data_H = (val>>8)&0xFF;
			U8T_data_L = (val)&0xFF;

		}//fi
	}//fi

}

void ht_init()
{
}

#endif