

#include "zcl.h"
#include "Global_defines.h"

#if defined (HAL_DATA_LOGGER_CHINA)
extern uint8  zclhtbatdata;
extern SimpleDescriptionFormat_t zcl_HtDesc;
extern CONST zclAttrRec_t zclHt_Attrs[];
extern uint8 zclHumidData[];
extern uint8 zclTempData[];
extern uint8 zclCounter_data_logger[];
extern uint8  U8T_data_H,U8T_data_L,U8RH_data_H,U8RH_data_L;

#define HT_MAX_ATTRIBUTES          5
#define HT_ENDPOINT            13

static zclReportCmd_t *pHtReportCmd;                               // report command structure for SE Cluster
static uint8 numHtAttr = HT_MAX_ATTRIBUTES-1;                                        // number of SE Cluster attributes in report

/****************************************************************************/
//H&T Aosong Sensor Specific Code
/****************************************************************************/
void Delay_10us(void);
void Delay(uint16);
void COM(void);
void RH(void);
void ht_init(void);

#endif


