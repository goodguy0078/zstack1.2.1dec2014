
void I2C_Clock(void);
void I2C_Start(void);
void I2C_Stop(void);
void I2C_Write(unsigned char dat);
unsigned char I2C_Read(void);
void I2C_Ack(void);
void I2C_NoAck(void);

