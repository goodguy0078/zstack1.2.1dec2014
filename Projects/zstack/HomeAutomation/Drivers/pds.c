

#if (defined HAL_PDS)
#include "pds.h"
#include "hal_mcu.h"
#include "hal_defs.h"
#include "hal_types.h"
#include "comdef.h"
#include "OSAL.h"
#include "ZComDef.h"
#include "hal_led.h"

uint8 Send_Pds(uint8);
uint8 RegisterForpds( uint8 );

static halPdsCBack_t pHalPdsProcessFunction;
// Registered keys task ID, initialized to NOT USED.
static uint8 registeredpdsTaskID = NO_TASK_ID;
bool Hal_PdsIntEnable;            /* interrupt enable/disable flag */

//Function declarations
void HalPdsConfig (bool, halPdsCBack_t, uint8 zclDimmer_TaskID);
void HalPdsConfigQuad (bool, halPdsCBack_t, uint8 zclDimmer_TaskID);
void pds_init(uint8, bool);
void halProcessPdsInterrupt (uint8);

void pds_init(uint8 zclDimmer_TaskID, bool Config)
{
	if (Config == 1)
	HalPdsConfigQuad(HAL_PDS_INTERRUPT_ENABLE, Send_Pds, zclDimmer_TaskID);
	else
	HalPdsConfig(HAL_PDS_INTERRUPT_ENABLE, Send_Pds, zclDimmer_TaskID);
}
/*********************************************************************
* PDS Register function
*
* The pda handler is setup to send all pds changes to
* one task (if a task is registered).
*
* If a task registers, it will get all the keys. You can change this
* to register for pds.
*********************************************************************/
uint8 RegisterForpds( uint8 task_id )
{
	// Allow only the first task
	if ( registeredpdsTaskID == NO_TASK_ID )
	{
		registeredpdsTaskID = task_id;
		return ( true );
	}
	else
	return ( false );
}

/*********************************************************************
* @fn      send_pds
*
* @brief   Send "pds" message to application.
*
* @param   state
*          
*
* @return  status
*********************************************************************/
uint8 Send_Pds( uint8 pds_no )
{
	pdsChange_t *msgPtr;

	if ( registeredpdsTaskID != NO_TASK_ID )
	{
		// Send the address to the task
		msgPtr = (pdsChange_t *)osal_msg_allocate( sizeof(pdsChange_t) );
		if ( msgPtr )
		{
			msgPtr->hdr.event = HAL_PDS_EVENT;
			msgPtr->state = pds_no;
			osal_msg_send( registeredpdsTaskID, (uint8 *)msgPtr );
		}
		return ( ZSuccess );
	}
	else
	return ( ZFailure );

}

/**************************************************************************************************
* @fn      HalPdsConfig
*
* @brief   Configure the Key serivce
*
* @param   interruptEnable - TRUE/FALSE, enable/disable interrupt
*          cback - pointer to the CallBack function
*
* @return  None
**************************************************************************************************/
void HalPdsConfig (bool interruptEnable, halPdsCBack_t cback, uint8 App_TaskID)
{
	/* Register the callback fucntion */
	pHalPdsProcessFunction = cback;

	/* Determine if interrupt is enable or not */
	/* Rising/Falling edge configuratinn */

	PICTL &= ~(HAL_PDS_EDGEBIT);    /* Clear the edge bit */
	/* For falling edge, the bit must be set. */
#if (HAL_PDS_EDGE == HAL_PDS_FALLING_EDGE)
	PICTL |= HAL_PDS_EDGEBIT;
#endif

	/* Interrupt configuration:
	* - Enable interrupt generation at the port
	* - Enable CPU interrupt
	* - Clear any pending interrupt
	*/
	HAL_PDS_ICTL |= HAL_PDS_ICTLBIT;
	HAL_PDS_IEN |= HAL_PDS_IENBIT;
	HAL_PDS_PXIFG = (uint8)~(HAL_PDS_BIT);
        HAL_PDS_CPU_PORT_1_IF = 0;


}

/**************************************************************************************************
* @fn      HalPdsConfig
*
* @brief   Configure the Key serivce
*
* @param   interruptEnable - TRUE/FALSE, enable/disable interrupt
*          cback - pointer to the CallBack function
*
* @return  None
**************************************************************************************************/
void HalPdsConfigQuad (bool interruptEnable, halPdsCBack_t cback, uint8 App_TaskID)
{
	/* Register the callback fucntion */
	pHalPdsProcessFunction = cback;

	/* Determine if interrupt is enable or not */
	/* Rising/Falling edge configuratinn */
	//p1.5
	PICTL &= ~(HAL_PDS_EDGEBIT);    /* Clear the edge bit */
	/* For falling edge, the bit must be set. */
#if (HAL_PDS_EDGE == HAL_PDS_FALLING_EDGE)
	PICTL |= HAL_PDS_EDGEBIT;
#endif

	/* Interrupt configuration:
	* - Enable interrupt generation at the port
	* - Enable CPU interrupt
	* - Clear any pending interrupt
	*/
	HAL_PDS_ICTL |= HAL_PDS_ICTLBIT;
	HAL_PDS_IEN |= HAL_PDS_IENBIT;
	HAL_PDS_PXIFG = (uint8)~(HAL_PDS_BIT);

	//p1.6    
	PICTL &= ~(HAL_PDS2_EDGEBIT);    /* Clear the edge bit */
	/* For falling edge, the bit must be set. */
#if (HAL_PDS2_EDGE == HAL_PDS2_FALLING_EDGE)
	PICTL |= HAL_PDS2_EDGEBIT;
#endif

	/* Interrupt configuration:
	* - Enable interrupt generation at the port
	* - Enable CPU interrupt
	* - Clear any pending interrupt
	*/
	HAL_PDS2_ICTL |= HAL_PDS2_ICTLBIT;
	HAL_PDS2_IEN |= HAL_PDS2_IENBIT;
	HAL_PDS2_PXIFG = (uint8)~(HAL_PDS2_BIT);

	//p1.7
	PICTL &= ~(HAL_PDS3_EDGEBIT);    /* Clear the edge bit */
	/* For falling edge, the bit must be set. */
#if (HAL_PDS3_EDGE == HAL_PDS3_FALLING_EDGE)
	PICTL |= HAL_PDS3_EDGEBIT;
#endif

	/* Interrupt configuration:
	* - Enable interrupt generation at the port
	* - Enable CPU interrupt
	* - Clear any pending interrupt
	*/
	HAL_PDS3_ICTL |= HAL_PDS3_ICTLBIT;
	HAL_PDS3_IEN |= HAL_PDS3_IENBIT;
	HAL_PDS3_PXIFG = (uint8)~(HAL_PDS3_BIT);

	//p2.0
	PICTL &= ~(HAL_PDS4_EDGEBIT);    /* Clear the edge bit */
	/* For falling edge, the bit must be set. */
#if (HAL_PDS4_EDGE == HAL_PDS4_FALLING_EDGE)
	PICTL |= HAL_PDS4_EDGEBIT;
#endif

	/* Interrupt configuration:
	* - Enable interrupt generation at the port
	* - Enable CPU interrupt
	* - Clear any pending interrupt
	*/
	HAL_PDS4_ICTL |= HAL_PDS4_ICTLBIT;
	HAL_PDS4_IEN |= HAL_PDS4_IENBIT;
	HAL_PDS4_PXIFG = (uint8)~(HAL_PDS4_BIT);

    HAL_PDS_CPU_PORT_1_IF = 0;
    HAL_PDS2_CPU_PORT_1_IF = 0;
    HAL_PDS3_CPU_PORT_1_IF = 0;
    HAL_PDS4_CPU_PORT_1_IF = 0;
}

/**************************************************************************************************
* @fn      halProcessPdsInterrupt
*
* @brief   Checks to see if it's a valid pds interrupt
*
* @param
*
* @return
**************************************************************************************************/
inline void halProcessPdsInterrupt (uint8 pds_no)
{
	switch (pds_no)
	{
	case 0:
		if (HAL_PDS_PXIFG & HAL_PDS_BIT)  /* Interrupt Flag has been set */
		{
			HAL_PDS_PXIFG = (uint8)~(HAL_PDS_BIT); /* Clear Interrupt Flag */
			/* Invoke Callback if new keys were depressed */
			if (pHalPdsProcessFunction)
			{
				(pHalPdsProcessFunction) (pds_no);
				//  HalLedSet ( HAL_LED_1, HAL_LED_MODE_TOGGLE);
			}
			
		}
		break;
	case 1:
		if (HAL_PDS2_PXIFG & HAL_PDS2_BIT)  /* Interrupt Flag has been set */
		{
			HAL_PDS2_PXIFG = ~(HAL_PDS2_BIT); /* Clear Interrupt Flag */
			/* Invoke Callback if new keys were depressed */
			if (pHalPdsProcessFunction)
			{
				(pHalPdsProcessFunction) (pds_no);
				//  HalLedSet ( HAL_LED_1, HAL_LED_MODE_TOGGLE);
			}
			
		}
		break;
	case 2:
		if (HAL_PDS3_PXIFG & HAL_PDS3_BIT)  /* Interrupt Flag has been set */
		{
			HAL_PDS3_PXIFG = ~(HAL_PDS3_BIT); /* Clear Interrupt Flag */
			/* Invoke Callback if new keys were depressed */
			if (pHalPdsProcessFunction)
			{
				(pHalPdsProcessFunction) (pds_no);
				//  HalLedSet ( HAL_LED_1, HAL_LED_MODE_TOGGLE);
			}
			
		}
		break;
	case 3:
		if (HAL_PDS4_PXIFG & HAL_PDS4_BIT)  /* Interrupt Flag has been set */
		{
			HAL_PDS4_PXIFG = ~(HAL_PDS4_BIT); /* Clear Interrupt Flag */
			/* Invoke Callback if new keys were depressed */
			if (pHalPdsProcessFunction)
			{
				(pHalPdsProcessFunction) (pds_no);
				//  HalLedSet ( HAL_LED_1, HAL_LED_MODE_TOGGLE);
			}
			
		}
		break;
	}
}

/**************************************************************************************************
* @fn      halPDSPort1Isr
*
* @brief   Port2 ISR
*
* @param
*
* @return
**************************************************************************************************/
HAL_ISR_FUNCTION( halKeyPort1Isr, P1INT_VECTOR )
{
	HAL_ENTER_ISR();

	if (HAL_PDS_PXIFG & HAL_PDS_BIT)
	{
		halProcessPdsInterrupt(PDS0);
		HAL_PDS_PXIFG = 0;
		HAL_PDS_CPU_PORT_1_IF = 0;
	}
	if (HAL_PDS2_PXIFG & HAL_PDS2_BIT)
	{
		halProcessPdsInterrupt(PDS1);
		HAL_PDS2_PXIFG = 0;
		HAL_PDS_CPU_PORT_1_IF = 0;
	}
	if (HAL_PDS3_PXIFG & HAL_PDS3_BIT)
	{
		halProcessPdsInterrupt(PDS2);
		HAL_PDS3_PXIFG = 0;
		HAL_PDS_CPU_PORT_1_IF = 0;
	}
        if (HAL_PDS4_PXIFG & HAL_PDS4_BIT)
	{
		halProcessPdsInterrupt(PDS3);
		HAL_PDS4_PXIFG = 0;
		HAL_PDS_CPU_PORT_1_IF = 0;
	}

	/*
	Clear the CPU interrupt flag for Port_1
	PxIFG has to be cleared before PxIF
*/
	CLEAR_SLEEP_MODE();
	HAL_EXIT_ISR();
}
#endif  //HAL_PDS
