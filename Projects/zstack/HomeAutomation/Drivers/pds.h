#include "hal_defs.h"
#include "hal_types.h"
#include "comdef.h"
#include "OSAL.h"

typedef uint8 (*halPdsCBack_t) (uint8 state);

#define PDS0              0
#define PDS1             1
#define PDS2             2
#define PDS3             3

#define HAL_PDS_EVENT                       0x0020
#define PDS_OU_INTRERVAL                  60000
#define PDS_FIRST_INTERVAL                 20000
#define PDS_OCCUPANCY_INTERVAL                 100

#define OTOUDELAY       PDS_OU_INTRERVAL/PDS_FIRST_INTERVAL

#define OCCUPIED                            TRUE
#define UNOCCUPIED                          FALSE
// Task ID not initialized

#define NO_TASK_ID 0xFF

//PDS 1.5
/* CPU port interrupt */
#define HAL_PDS_CPU_PORT_1_IF P1IF

/* Interrupt option - Enable or disable */
#define HAL_PDS_INTERRUPT_DISABLE    0x00
#define HAL_PDS_INTERRUPT_ENABLE     0x01

#define HAL_PDS_RISING_EDGE   0
#define HAL_PDS_FALLING_EDGE  1

/* edge interrupt */

/* PDS is at P1.5 */
#define HAL_PDS_PORT   P1
#define HAL_PDS_BIT    BV(5)
#define HAL_PDS_SEL    P1SEL
#define HAL_PDS_DIR    P1DIR

/* edge interrupt */
#define HAL_PDS_EDGEBIT  BV(2) //Based on PICTL Reg
#define HAL_PDS_EDGE     HAL_PDS_FALLING_EDGE


/* SW_6 interrupts */
#define HAL_PDS_IEN      IEN2  /* CPU interrupt mask register */
#define HAL_PDS_IENBIT   BV(4) /* //Based on IEN2 Reg*/
#define HAL_PDS_ICTL     P1IEN /* Port Interrupt Control register */
#define HAL_PDS_ICTLBIT  BV(5) /* P1IEN - P1.2 enable/disable bit */
#define HAL_PDS_PXIFG    P1IFG /* Interrupt flag at source */

//PDS 1.6
/* CPU port interrupt */
#define HAL_PDS2_CPU_PORT_1_IF P1IF

/* Interrupt option - Enable or disable */
#define HAL_PDS2_INTERRUPT_DISABLE    0x00
#define HAL_PDS2_INTERRUPT_ENABLE     0x01

#define HAL_PDS2_RISING_EDGE   0
#define HAL_PDS2_FALLING_EDGE  1

/* PDS is at P1.6 */
#define HAL_PDS2_PORT   P1
#define HAL_PDS2_BIT    BV(6)
#define HAL_PDS2_SEL    P1SEL
#define HAL_PDS2_DIR    P1DIR

/* edge interrupt */
#define HAL_PDS2_EDGEBIT  BV(2)
#define HAL_PDS2_EDGE     HAL_PDS_FALLING_EDGE

/* SW_6 interrupts */
#define HAL_PDS2_IEN      IEN2  /* CPU interrupt mask register */
#define HAL_PDS2_IENBIT   BV(4) /* Mask bit for all of Port_2 */
#define HAL_PDS2_ICTL     P1IEN /* Port Interrupt Control register */
#define HAL_PDS2_ICTLBIT  BV(6) /* P2IEN - P2.0 enable/disable bit */
#define HAL_PDS2_PXIFG    P1IFG /* Interrupt flag at source */

//PDS 3 1.7
/* CPU port interrupt */
#define HAL_PDS3_CPU_PORT_1_IF P1IF

/* Interrupt option - Enable or disable */
#define HAL_PDS3_INTERRUPT_DISABLE    0x00
#define HAL_PDS3_INTERRUPT_ENABLE     0x01

#define HAL_PDS3_RISING_EDGE   0
#define HAL_PDS3_FALLING_EDGE  1

/* PDS is at P1.7 */
#define HAL_PDS3_PORT   P1
#define HAL_PDS3_BIT    (uint8)BV(7)
#define HAL_PDS3_SEL    P1SEL
#define HAL_PDS3_DIR    P1DIR

/* edge interrupt */
#define HAL_PDS3_EDGEBIT  BV(2)
#define HAL_PDS3_EDGE     HAL_PDS_FALLING_EDGE

/* SW_6 interrupts */
#define HAL_PDS3_IEN      IEN2  /* CPU interrupt mask register */
#define HAL_PDS3_IENBIT   BV(4) /* Mask bit for all of Port_1 */
#define HAL_PDS3_ICTL     P1IEN /* Port Interrupt Control register */
#define HAL_PDS3_ICTLBIT  BV(7) /* P2IEN - P2.0 enable/disable bit */
#define HAL_PDS3_PXIFG    P1IFG /* Interrupt flag at source */

//PDS 4
/* CPU port interrupt */
#define HAL_PDS4_CPU_PORT_1_IF P1IF
/* Interrupt option - Enable or disable */
#define HAL_PDS4_INTERRUPT_DISABLE    0x00
#define HAL_PDS4_INTERRUPT_ENABLE     0x01

#define HAL_PDS4_RISING_EDGE   0
#define HAL_PDS4_FALLING_EDGE  1

/* PDS is at P1.3 */
#define HAL_PDS4_PORT   P1
#define HAL_PDS4_BIT    BV(3)
#define HAL_PDS4_SEL    P1SEL
#define HAL_PDS4_DIR    P1DIR

/* edge interrupt */
#define HAL_PDS4_EDGEBIT  BV(1)
#define HAL_PDS4_EDGE     HAL_PDS_FALLING_EDGE

/* SW_6 interrupts */
#define HAL_PDS4_IEN      IEN2  /* CPU interrupt mask register */
#define HAL_PDS4_IENBIT   BV(4) /* Mask bit for all of Port_2 */
#define HAL_PDS4_ICTL     P1IEN /* Port Interrupt Control register */
#define HAL_PDS4_ICTLBIT  BV(3) /* P2IEN - P2.0 enable/disable bit */
#define HAL_PDS4_PXIFG    P1IFG /* Interrupt flag at source */

/*
* Configure the Pds Service
*/
extern void HalPdsConfig( bool interruptEnable, const halPdsCBack_t cback, uint8 zclDimmer_TaskID);
extern uint8 RegisterForpds( uint8 );
extern void pds_init(uint8, bool);

/* structure definitions */

typedef struct
{
	osal_event_hdr_t hdr;
	uint8 state; //presece detected or not
} pdsChange_t;