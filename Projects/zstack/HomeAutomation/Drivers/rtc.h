void DS1307_Init(void);
void DS1307_Write(unsigned char dat);
unsigned char DS1307_Read(void);
void DS1307_SetTime(unsigned char hh, unsigned char mm, unsigned char ss);
void DS1307_SetDate(unsigned char dd, unsigned char mm, unsigned char yy);
uint8 DS1307_GetTime(unsigned char *h_ptr,unsigned char *m_ptr,unsigned char *s_ptr);
void DS1307_GetDate(unsigned char *d_ptr,unsigned char *m_ptr,unsigned char *y_ptr);
void DS1307_GetStatus(unsigned char *status);