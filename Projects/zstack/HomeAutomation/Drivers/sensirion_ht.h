#if defined (HAL_DATA_LOGGER_8051)

#include "zcl.h"
#define HT_MAX_ATTRIBUTES          5
#define HT_ENDPOINT            13
#define   _nop_()   Onboard_wait(5);

extern uint8  zclhtbatdata[2];
extern SimpleDescriptionFormat_t zcl_HtDesc;
extern CONST zclAttrRec_t zclHt_Attrs[];
extern uint8 zclHumidData[];
extern uint8 zclTempData[];
extern uint8 zclCounter_data_logger[];
extern uint8  U8T_data_H,U8T_data_L,U8RH_data_H,U8RH_data_L;

static zclReportCmd_t *pHtReportCmd;                               // report command structure for SE Cluster
static uint8 numHtAttr = HT_MAX_ATTRIBUTES-1;                                        // number of SE Cluster attributes in report

void RH(void);
void ht_init(void);
float calc_dewpoint(float h,float t);
void calc_sth11(float *p_humidity ,float *p_temperature);
char s_measure(unsigned char *p_value, unsigned char *p_checksum, unsigned char mode);
char s_write_statusreg(unsigned char *p_value);
char s_read_statusreg(unsigned char *p_value, unsigned char *p_checksum);
char s_softreset(void);
void s_transstart(void);
char s_read_byte(unsigned char ack);
char s_write_byte(unsigned char value);
void s_connectionreset(void);

#endif
