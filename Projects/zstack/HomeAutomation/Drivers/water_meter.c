
#include "water_meter.h"
#include "hal_mcu.h"
#include "hal_defs.h"
#include "hal_types.h"
#include "comdef.h"
#include "OSAL.h"
#include "ZComDef.h"
#include "hal_led.h"

#if (defined HAL_WM) 
uint8 Send_Wm(void);
uint8 RegisterForwm( uint8 );

static halWmCBack_t pHalWmProcessFunction;
// Registered keys task ID, initialized to NOT USED.
static uint8 registeredwmTaskID = NO_TASK_ID;
bool Hal_WmIntEnable;            /* interrupt enable/disable flag */

//Function declarations
void HalWmConfig (bool, halWmCBack_t, uint8 zclDimmer_TaskID);
void HalWmInit( void );
void wm_init(uint8);
void halProcessWmInterrupt(void);

void wm_init(uint8 zclDimmer_TaskID)
{
	HalWmConfig(HAL_WM_INTERRUPT_ENABLE , Send_Wm, zclDimmer_TaskID);
}
/*********************************************************************
* WM Register function
*
* The pda handler is setup to send all wm changes to
* one task (if a task is registered).
*
* If a task registers, it will get all the keys. You can change this
* to register for wm.
*********************************************************************/
uint8 RegisterForwm( uint8 task_id )
{
	// Allow only the first task
	if ( registeredwmTaskID == NO_TASK_ID )
	{
		registeredwmTaskID = task_id;
		return ( true );
	}
	else
	return ( false );
}

/*********************************************************************
* @fn      send_wm
*
* @brief   Send "wm" message to application.
*
* @param   state
*          
*
* @return  status
*********************************************************************/
uint8 Send_Wm(void)
{
	wmChange_t *msgPtr;

	if ( registeredwmTaskID != NO_TASK_ID )
	{
		// Send the address to the task
		msgPtr = (wmChange_t *)osal_msg_allocate( sizeof(wmChange_t) );
		if ( msgPtr )
		{
			msgPtr->hdr.event = WM_CHANGE;
			osal_msg_send( registeredwmTaskID, (uint8 *)msgPtr );
		}
		return ( ZSuccess );
	}
	else
	return ( ZFailure );

}

/**************************************************************************************************
* @fn      HalWmInit
*
* @brief   Initilize Key Service
*
* @param   none
*
* @return  None
**************************************************************************************************/
void HalWmInit( void )
{

	HAL_WM_SEL &= ~(HAL_WM_BIT);    /* Set pin function to GPIO */
	HAL_WM_DIR &= ~(HAL_WM_BIT);    /* Set pin direction to Input */
	/* Initialize callback function */
	pHalWmProcessFunction  = NULL;

}


/**************************************************************************************************
* @fn      HalWmConfig
*
* @brief   Configure the Key serivce
*
* @param   interruptEnable - TRUE/FALSE, enable/disable interrupt
*          cback - pointer to the CallBack function
*
* @return  None
**************************************************************************************************/
void HalWmConfig (bool interruptEnable, halWmCBack_t cback, uint8 App_TaskID)
{
	/* Register the callback fucntion */
	pHalWmProcessFunction = cback;

	/* Determine if interrupt is enable or not */
	/* Rising/Falling edge configuratinn */

	PICTL &= ~(HAL_WM_EDGEBIT);    /* Clear the edge bit */
	/* For falling edge, the bit must be set. */
#if (HAL_WM_EDGE == HAL_WM_FALLING_EDGE)
	PICTL |= HAL_WM_EDGEBIT;
#endif

	/* Interrupt configuration:
	* - Enable interrupt generation at the port
	* - Enable CPU interrupt
	* - Clear any pending interrupt
	*/
	HAL_WM_ICTL |= HAL_WM_ICTLBIT;
	HAL_WM_IEN |= HAL_WM_IENBIT;
	HAL_WM_PXIFG = (uint8)~(HAL_WM_BIT);
	
}


/**************************************************************************************************
* @fn      halProcessWmInterrupt
*
* @brief   Checks to see if it's a valid wm interrupt
*
* @param
*
* @return
**************************************************************************************************/
void halProcessWmInterrupt(void)
{
	if (HAL_WM_PXIFG & HAL_WM_BIT)  /* Interrupt Flag has been set */
	{
		HAL_WM_PXIFG = (uint8)~(HAL_WM_BIT); /* Clear Interrupt Flag */
		/* Invoke Callback if new keys were depressed */
		if (pHalWmProcessFunction)
		{
			(pHalWmProcessFunction)();
			//  HalLedSet ( HAL_LED_1, HAL_LED_MODE_TOGGLE);
		}
	}
}

/**************************************************************************************************
* @fn      halWMPort1Isr
*
* @brief   Port2 ISR
*
* @param
*
* @return
**************************************************************************************************/
HAL_ISR_FUNCTION( halKeyPort1Isr, P1INT_VECTOR )
{
	HAL_ENTER_ISR();
	static uint16 count_wm=0;
	if (HAL_WM_PXIFG & HAL_WM_BIT)
	{
		count_wm++;
		if (count_wm == WM_LITRE_COUNT)
		{
			halProcessWmInterrupt();
			count_wm=0;
		}
		HAL_WM_PXIFG = 0;
		HAL_WM_CPU_PORT_1_IF = 0;
	}

	/*
	Clear the CPU interrupt flag for Port_1
	PxIFG has to be cleared before PxIF
*/
	CLEAR_SLEEP_MODE();
	HAL_EXIT_ISR();
}
#endif  //HAL_WM