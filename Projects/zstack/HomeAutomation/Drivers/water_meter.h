#include "hal_defs.h"
#include "hal_types.h"
#include "comdef.h"
#include "OSAL.h"

typedef uint8 (*halWmCBack_t) (void);

#define HAL_WM_EVENT                       0x0100
#define WM_OU_INTRERVAL                  60000
#define WM_FIRST_INTERVAL                 20000
#define WM_OCCUPANCY_INTERVAL                 100
#define WM_LITRE_COUNT					045

// Task ID not initialized

#define NO_TASK_ID 0xFF
#define WM_CHANGE                0xE0    // Key Events

//WM 1.3
/* CPU port interrupt */
#define HAL_WM_CPU_PORT_1_IF P1IF

/* Interrupt option - Enable or disable */
#define HAL_WM_INTERRUPT_DISABLE    0x00
#define HAL_WM_INTERRUPT_ENABLE     0x01

#define HAL_WM_RISING_EDGE   0
#define HAL_WM_FALLING_EDGE  1

/* edge interrupt */

/* WM is at P1.3 */
#define HAL_WM_PORT   P1
#define HAL_WM_BIT    BV(3)
#define HAL_WM_SEL    P1SEL
#define HAL_WM_DIR    P1DIR

/* edge interrupt */
#define HAL_WM_EDGEBIT  BV(1) //Based on PICTL Reg
#define HAL_WM_EDGE     HAL_WM_FALLING_EDGE


/* SW_6 interrupts */
#define HAL_WM_IEN      IEN2  /* CPU interrupt mask register */
#define HAL_WM_IENBIT   BV(4) /* //Based on IEN2 Reg*/
#define HAL_WM_ICTL     P1IEN /* Port Interrupt Control register */
#define HAL_WM_ICTLBIT  BV(3) /* P1IEN - P1.2 enable/disable bit */
#define HAL_WM_PXIFG    P1IFG /* Interrupt flag at source */

/*
* Configure the Wm Service
*/
extern void HalWmConfig( bool interruptEnable, const halWmCBack_t cback, uint8 zclDimmer_TaskID);
extern void HalWmInit( void );
extern uint8 RegisterForwm( uint8 );
extern void wm_init(uint8);

/* structure definitions */

typedef struct
{
  osal_event_hdr_t hdr;
} wmChange_t;