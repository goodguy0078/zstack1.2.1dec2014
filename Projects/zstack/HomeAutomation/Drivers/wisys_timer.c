/**************************************************************************************************
Filename:       hal_timer.c
Revised:        $Date: 2010-05-28 15:26:34 -0700 (Fri, 28 May 2010) $
Revision:       $Revision: 22676 $

Description:   This file contains the interface to the Timer Service.


Copyright 2006-2010 Texas Instruments Incorporated. All rights reserved.

IMPORTANT: Your use of this Software is limited to those specific rights
granted under the terms of a software license agreement between the user
who downloaded the software, his/her employer (which must be your employer)
and Texas Instruments Incorporated (the "License").  You may not use this
Software unless you agree to abide by the terms of the License. The License
limits your use, and you acknowledge, that the Software may not be modified,
copied or distributed unless embedded on a Texas Instruments microcontroller
or used solely and exclusively in conjunction with a Texas Instruments radio
frequency transceiver, which is integrated into your product.  Other than for
the foregoing purpose, you may not use, reproduce, copy, prepare derivative
works of, modify, distribute, perform, display or sell this Software and/or
its documentation for any purpose.

YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

Should you have any questions regarding your right to use this Software,
contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/*********************************************************************
NOTE: Z-Stack and TIMAC no longer use CC2530 Timer 1, Timer 3, and 
	Timer 4. The supporting timer driver module is removed and left 
	for the users to implement their own application timer 
	functions.
*********************************************************************/
#include "Global_defines.h"
#include "hal_mcu.h"
#include "wisys_timer.h"
#include "hal_types.h"
#include "hal_defs.h"
#include "hal_led.h"
#include "onboard.h"

#define GRAD_STEP 5

#if defined (HAL_PWM_PYROTECH)
#define PWM_T1CTL 0xf
#define PWM_T1CC0L 0x71
#define PWM_T1CC0H 0x2
#define PWM_P0SEL 0x8
#define PWM_DC	99
#define PWM_MULT 6

#elif defined (HAL_PWM_SIMOCO)
#define PWM_T1CTL 0x7
#define PWM_T1CC0L 0xe0
#define PWM_T1CC0H 0x08
#define PWM_P0SEL 0x8
#define PWM_DC	99
#define PWM_MULT 22

#elif defined (HAL_PWM_SUKAM)
#define PWM_T1CTL 0x3
#define PWM_T1CC0L 0x34
#define PWM_T1CC0H 0x03
#define PWM_P0SEL 0x8
#define PWM_DC	99
#define PWM_MULT 8

#else
#define PWM_T1CTL 0x3
#define PWM_T1CC0L 0xf4
#define PWM_T1CC0H 0x1
#define PWM_P0SEL 0x10
#define PWM_DC	0
#define PWM_MULT 5
#endif


#if defined (HAL_DIMMER) || defined (HAL_DIMMER_QUAD)
uint8 UpdateDutyCycle(uint8, bool, uint8);
void UpdateDutyCycle_grad(uint8 dc, bool emer_flag, uint8 ctrl_no, uint8 old_dc);
void InitPWM(bool flag);
static uint8 inline Nxt_High_Grad(uint8 val);
static uint8 inline Nxt_Low_Grad(uint8 val);
static void DelayMicroSeconds (uint16 us);
void DelayMilliSeconds (uint16 us);

static void DelayMicroSeconds (uint16 us)
{
	Onboard_wait(us);
}

void DelayMilliSeconds (uint16 us)
{
	while(--us){
		for (uint16 i=0; i<59; i++)
		DelayMicroSeconds(1);
	}
}
#define PWM_Pin         P2_3

#if defined (HAL_DIMMER_QUAD) || defined (HAL_DIMMER)
uint8 inline Nxt_High_Grad(uint8 val)
{
	return ((val - (val % GRAD_STEP)) + GRAD_STEP);
}

uint8 inline Nxt_Low_Grad(uint8 val)
{
	return ((val - (val % GRAD_STEP)) - GRAD_STEP);
}

void UpdateDutyCycle_grad(uint8 dc, bool emer_flag, uint8 ctrl_no, uint8 old_dc){
	if (dc==old_dc)
		UpdateDutyCycle(dc, emer_flag, ctrl_no);
	else if (dc > old_dc){
		for (; dc > old_dc ;){
			if (Nxt_High_Grad(old_dc) > dc){
				UpdateDutyCycle(dc, emer_flag, ctrl_no);
				break;		//This should be last call
			}
			else{
				old_dc = Nxt_High_Grad(old_dc);
				UpdateDutyCycle(old_dc, emer_flag, ctrl_no);
			}
			DelayMilliSeconds(2);                 
		}
	}
	else{
		for (; dc < old_dc;){
			if (Nxt_Low_Grad(old_dc) < dc){
				UpdateDutyCycle(dc,emer_flag, ctrl_no);
				break;		//This should be last call
			}
			else{
				old_dc = Nxt_Low_Grad(old_dc);
				UpdateDutyCycle(old_dc, emer_flag, ctrl_no);
			}
			DelayMilliSeconds(2);
		}           
	}
	
}
#endif

#if defined (HAL_DIMMER_SYNCO)  || defined (HAL_DIMMER_PYROTECH)
uint8 UpdateDutyCycle(uint8 dc, bool emer_flag, uint8 ctrl_no)
{
	if(dc == 100)
	dc = PWM_DC;
	else if(dc == 0)  //  COMMENT OUT FOR LIGHTS CUBE
	dc = 100;
	dc = 100-dc; 
        uint16 temp;
//       if(dc)
//          dc=100;  //for lights cube
	//   T1CCTL0 = 0x1c; // Channel 0 in compare mode, Set output on compare-up, clear on 0 (50% duty cycle)
	if(!emer_flag){
		T1CTL = PWM_T1CTL; // divide with 128 and to do i up-down mode
		temp = PWM_MULT*dc;
		T1CC0L = PWM_T1CC0L; // PWM signal period
		T1CC0H = PWM_T1CC0H;
		switch(ctrl_no)
		{
		case 0:
			T1CC1L = temp & 0xFF; // PWM duty cycle, Channel 1 (P0_3)
			T1CC1H = (temp >> 8);
			T1CCTL1 = 0x1c; // Channel 1 in compare mode, Set output on compare-up, clear on compare-down
			break;
			
		case 1:
			T1CC2L = temp & 0xFF; // PWM duty cycle, Channel 2 (P0_4)
			T1CC2H = (temp >> 8);
			T1CCTL2 = 0x1c; // Channel 2 in compare mode, Set output on compare-up, clear on compare-down
			break;
			
		case 2:
			T1CC3L = temp & 0xFF; // PWM duty cycle, Channel 3 (P0_5)
			T1CC3H = (temp >> 8);
			T1CCTL3 = 0x1c; // Channel 3 in compare mode, Set output on compare-up, clear on compare-down
			break;
			
		case 3:
			T1CC4L = temp & 0xFF; // PWM duty cycle, Channel 4 (P0_6)
			T1CC4H = (temp >> 8);;
			T1CCTL4 = 0x1c; // Channel 4 in compare mode, Set output on compare-up, clear on compare-down
			break;
		}      
	}

	else{
		T1CTL = 0x0f; // divide with 128 and to do i up-down mode
		T1CC0L = 0x50; // Put emergency cycle period here for 1 sec
		T1CC0H = 0xc3;
		temp = 500 * dc;
		switch(ctrl_no)
		{
		case 0:
			T1CC1L = temp & 0xFF; // PWM duty cycle, Channel 1 (P0_4)
			T1CC1H = (temp >> 8);
			T1CCTL1 = 0x1c; // Channel 1 in compare mode, Set output on compare-up, clear on compare-down
			break;
			
		case 1:
			T1CC2L = temp & 0xFF; // PWM duty cycle, Channel 2 (P0_4)
			T1CC2H = (temp >> 8);
			T1CCTL2 = 0x1c; // Channel 2 in compare mode, Set output on compare-up, clear on compare-down
			break;
			
		case 2:
			T1CC3L = temp & 0xFF; // PWM duty cycle, Channel 3 (P0_5)
			T1CC3H = (temp >> 8);
			T1CCTL3 = 0x1c; // Channel 3 in compare mode, Set output on compare-up, clear on compare-down
			break;
			
		case 3:
			T1CC4L = temp & 0xFF; // PWM duty cycle, Channel 4 (P0_6)
			T1CC4H = (temp >> 8);;
			T1CCTL4 = 0x1c; // Channel 4 in compare mode, Set output on compare-up, clear on compare-down
			break;
		}    
	}
	return 0;
}
#endif
#if defined (HAL_DIMMER_AVNI)
uint8 UpdateDutyCycle(uint8 dc, bool emer_flag,uint8 ctrl_no)
{
	P0_3=1;
	if(dc == 100)
	dc = 0;
	else if(dc == 0)
	{
		dc = 100;
		P0_3= 0;
	}  
	uint16 temp;
	//  dc = 100-dc; //comment this for Shah and Syncolite
	if(!emer_flag){
		T1CC0L = 0xE8; // PWM signal period
		T1CC0H = 0x03;
		temp = 10*dc;
		T1CC2L = temp & 0xFF ; // PWM duty cycle
		T1CC2H = (temp >> 8);
		T1CTL = 0x07; 
	}
	else{
		T1CC0L = 0xff; // Put emergency cycle period here for 1 sec
		T1CC0H = 0xff;
		temp = 500*dc;
		T1CC2L = temp & 0xFF ; // PWM duty cycle
		T1CC2H = (temp >> 8);
		T1CTL = 0x0f; 
	}

	T1CCTL2 = 0x1c;
	return 0;
}
#endif
void InitPWM(bool quad_flag)
{
	if (quad_flag){
		PERCFG |= 0x03; // Move USART0 and USART1 to Alternative 2 location to allow all Timer 1 channels on P0
		P2DIR = (P2DIR & ~0xC0) | 0x80; // Give priority to Timer 1
		P0SEL |= 0x78; // Set P0_3-6 to peripheral  
	}
	else{
		PERCFG |= 0x03; // Move USART0 and USART1 to Alternative 2 location to allow all Timer 1 channels on P0
		P2DIR = (P2DIR & ~0xC0) | 0x80; // Give priority to Timer 1
		P0SEL |= PWM_P0SEL ; // Set P0_4 to peripheral
		P0DIR |= 0x08 ; // Set P0_3 to peripheral			//For Avni we need additional relay pin
	}
}
#endif
