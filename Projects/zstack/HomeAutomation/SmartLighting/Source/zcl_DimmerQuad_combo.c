/**************************************************************************************************
Filename:       zcl_sampleLight.c
Revised:        $Date: 2009-03-18 15:56:27 -0700 (Wed, 18 Mar 2009) $
Revision:       $Revision: 19453 $


Description:    Zigbee Cluster Library - sample device application.


Copyright 2006-2009 Texas Instruments Incorporated. All rights reserved.

IMPORTANT: Your use of this Software is limited to those specific rights
granted under the terms of a software license agreement between the user
who downloaded the software, his/her employer (which must be your employer)
and Texas Instruments Incorporated (the "License").  You may not use this
Software unless you agree to abide by the terms of the License. The License
limits your use, and you acknowledge, that the Software may not be modified,
copied or distributed unless embedded on a Texas Instruments microcontroller
or used solely and exclusively in conjunction with a Texas Instruments radio
frequency transceiver, which is integrated into your product.  Other than for
the foregoing purpose, you may not use, reproduce, copy, prepare derivative
works of, modify, distribute, perform, display or sell this Software and/or
its documentation for any purpose.

YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
PROVIDED “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

Should you have any questions regarding your right to use this Software,
contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/*********************************************************************
This device will be like a Light device.  This application is not
intended to be a Light device, but will use the device description
to implement this sample code.
*********************************************************************/

/*********************************************************************
* INCLUDES
*/
#include "Global_defines.h"
#include "ZComDef.h"
#include "OSAL.h"
#include "AF.h"
#include "ZDApp.h"
#include "Rtg.h"
#include "zcl.h"
#include "zcl_general.h"
#include "zcl_ha.h"
#include "zcl_ezmode.h"

#include "zcl_dimmer_quad.h"
#include "pds.h"

#include "zcl_ms.h"
#include "ZDObject.h"
#include "ZDProfile.h"
#include "OSAL_NV.h"

#include "onboard.h"
#include "sensirion_ht.h"
#include "HTC_Sensor.h"
/* HAL */
#include "hal_led.h"
#include "hal_key.h"
#include "hal_adc.h"
#include "wisys_timer.h"
#include "rtc.h"
#include "NLMEDE.h"
#include "ZDNwkMgr.h"

/*********************************************************************
* MACROS
*/
#if defined (HAL_RTC)
#define RTC_STATUS_OFFSET		2
#define RTC_TEST        	1
#define RTC_IN_CLUSTER	1
#else
#define RTC_IN_CLUSTER	0
#endif
	
// There is no attribute in the Mandatory Reportable Attribute list for now
#define zcl_MandatoryReportableAttribute( a ) ( a != NULL )

#define MT_ZDO_JOIN_CNF_LEN               							5
#define ZCLSAMPLELIGHT_BINDINGLIST       							3
#define EMERGENCY_ON  (bool)										1
#define EMERGENCY_OFF  (bool)										0
#define DEFAULT_DIMMING_FULL            							100
#define FLOOD_TIMING  												1000 //In ms
#define MAX_FLOOD_COUNT 											31
#define MAX_SCHEDULES   											10


/*********************************************************************
* CONSTANTS
*/
/*********************************************************************
* TYPEDEFS
*/

/*********************************************************************
* GLOBAL VARIABLES
*/
byte zclQDimmer_TaskID;
static afAddrType_t zclQDimmer_DstAddr;
static uint8 flood_fs_cntr = 0;
static bool flood_active = 0;
static uint32 newminReportInt = 0;
static uint32 minReport_32 = 0;
static uint16 parent_addr=0;
static zclReportCmd_t *pDimmerReportCmd[NUMBER_OF_LIGHT_PORTS];                               // report command structure for SE Cluster

extern void DelayMilliSeconds (uint16 us);

#ifdef ZCL_EZMODE
#define EZMODE_IN_CLUSTER	1
static bool pds_triggered[1];
#else
#define EZMODE_IN_CLUSTER	0
#endif
static uint8 numDimmerAttr = DIMMER_MAX_ATTRIBUTES-RTC_IN_CLUSTER-EZMODE_IN_CLUSTER;		//remove input attributes like rtc schedule etc								 

#if defined (HAL_PDS) || defined (ZCL_EZMODE)
static bool occupancy_override_flag = FALSE;
static bool pds_triggered[NUMBER_OF_LIGHT_PORTS];
static bool pds_last_state[NUMBER_OF_LIGHT_PORTS];
static uint8 zcldfltdemi = 50;
extern uint8 zclPdsData[NUMBER_OF_LIGHT_PORTS][2];
static uint16 otoucounts[NUMBER_OF_LIGHT_PORTS];
extern uint16 zclOtoUDelay;
static uint32 zclOtoUDelay_32 =300000;
#endif

/*********************************************************************
* EXTERN VARIABLES
*/

extern uint8 zclCounter_dimmer[NUMBER_OF_LIGHT_PORTS][5];
extern uint8 zclQDimmerData[NUMBER_OF_LIGHT_PORTS][2];
extern uint8 UpdateDutyCycle(uint8, bool, uint8);
extern void InitPWM(bool flag);
void prepare_send_select_epdata_dummy(uint8 ep_no);



/*********************************************************************
* LOCAL VARIABLES
*/
//static afAddrType_t zclDimmer_DstAddr;
struct dimmer_sched{
	unsigned char 	hh;
	unsigned char	mm;
	uint8 		dim_value;
};


#if defined (HAL_RTC_RST)

#define DEBUG	0
void check_inside_sm (void);
void convert_bcd_to_decimal (uint8 *hhc, uint8 *mmc, uint8 *ssc);

typedef enum
{
	init,
	start,
	data,
	stop
}sched_state;

#define MAX_SCHED_PER_NIGHT	6

struct sched_table{
	uint8 no_schedules;
	struct dimmer_sched dim_tim[MAX_SCHED_PER_NIGHT];
};

#define	MAX_SCHED_TABLES	1+ 9 + 1	//(1 extra for time sync + 1 extra for bypass)
//first element is max schedules
#if !DEBUG
struct	sched_table rtc_dim_sched [MAX_SCHED_TABLES] = {
									{{1}, {0x0,0x0,15}},	//Dummy- if idx is 0- bypass rtc mode
									{{MAX_SCHED_PER_NIGHT}, {0x0,0x0,0,0x9,0x30,25,0x11,0x30,50,0x13,0x30,75,0x15,0x30,100,0x17,0x30,0}},
									{{MAX_SCHED_PER_NIGHT}, {0x0,0x0,50,0x4,0x0,100,0x6,0x0,0,0x18,0x0,60,0x19,0x0,100,0x22,0x0,80}},
									{{MAX_SCHED_PER_NIGHT}, {0x0,0x0,60,0x4,0x0,100,0x6,0x30,0,0x18,0x30,60,0x19,0x0,100,0x22,0x0,80}},
									{{MAX_SCHED_PER_NIGHT}, {0x0,0x0,60,0x5,0x0,100,0x6,0x0,0,0x18,0x0,60,0x19,0x0,100,0x22,0x0,80}},
									{{MAX_SCHED_PER_NIGHT}, {0x0,0x0,60,0x5,0x0,100,0x6,0x30,0,0x18,0x30,60,0x19,0x0,100,0x22,0x0,80}},
									{{MAX_SCHED_PER_NIGHT}, {0x0,0x0,60,0x5,0x0,100,0x6,0x30,0,0x19,0x0,80,0x20,0x0,100,0x22,0x0,80}},
									{{MAX_SCHED_PER_NIGHT}, {0x0,0x0,80,0x4,0x0,100,0x6,0x0,0,0x18,0x0,60,0x19,0x0,100,0x22,0x0,80}},
									{{MAX_SCHED_PER_NIGHT}, {0x0,0x0,80,0x5,0x0,100,0x6,0x0,0,0x18,0x0,60,0x19,0x0,100,0x22,0x0,80}},
									{{MAX_SCHED_PER_NIGHT}, {0x0,0x0,80,0x5,0x0,100,0x6,0x0,0,0x18,0x0,60,0x19,0x0,100,0x22,0x0,80}},
									{{1}, {0x0,0x0,15}},	//Dummy- idx for rtc sync
								};

#else
struct	sched_table rtc_dim_sched [MAX_SCHED_TABLES] = {
									{{1}, {0x0,0x0,15}},	//Dummy- if idx is 0- bypass rtc mode
									{{MAX_SCHED_PER_NIGHT}, {0x15,0x2,10,0x15,0x4,30,0x15,0x6,50,0x15,0x8,70,0x15,0x10,90,0x15,0x12,80}},
									{{MAX_SCHED_PER_NIGHT}, {0x0,0x0,0,0x9,0x30,25,0x11,0x30,50,0x13,0x30,75,0x15,0x30,100,0x17,0x30,0}},
									{{MAX_SCHED_PER_NIGHT}, {0x0,0x0,50,0x4,0x0,100,0x6,0x0,0,0x18,0x0,60,0x19,0x0,100,0x22,0x0,80}},
									{{MAX_SCHED_PER_NIGHT}, {0x15,0x2,0,0x15,0x4,20,0x15,0x6,30,0x15,0x8,40,0x15,0x10,50,0x15,0x12,60}},
									{{MAX_SCHED_PER_NIGHT}, {0x0,0x0,60,0x5,0x0,100,0x6,0x0,0,0x18,0x0,60,0x19,0x0,100,0x22,0x0,80}},
									{{MAX_SCHED_PER_NIGHT}, {0x0,0x0,60,0x5,0x0,100,0x6,0x30,0,0x18,0x30,60,0x19,0x0,100,0x22,0x0,80}},
									{{MAX_SCHED_PER_NIGHT}, {0x0,0x0,60,0x5,0x0,100,0x6,0x30,0,0x19,0x0,80,0x20,0x0,100,0x22,0x0,80}},
									{{MAX_SCHED_PER_NIGHT}, {0x15,0x2,40,0x15,0x4,20,0x15,0x6,10,0x15,0x8,90,0x15,0x10,50,0x15,0x12,60}},
									{{MAX_SCHED_PER_NIGHT}, {0x15,0x2,20,0x15,0x4,30,0x15,0x6,40,0x15,0x8,50,0x15,0x10,60,0x15,0x12,70}},
									{{1}, {0x0,0x0,15}},	//Dummy- idx for rtc sync
								};

#endif

#endif	

#if defined (HAL_RTC)
static bool fail_safe_mode = FALSE;			//By default fail safe mode is off- assume  notwork connection such that we restore previous value
static uint8 cur_idx = 0;
static unsigned char hh=0, mm=0, ss=0;
static bool roll_ovr=0;
static uint8 fs_cntr=0;
static bool data_sent_flag;
#define MAX_FS_COUNT	3
#define SECOND_OFF 0x30					//No of seconds before 3 resets should be hit	

typedef struct node_time{
	unsigned char 	hh;
	unsigned char	mm;
	unsigned char	ss;
}Time;

#endif

struct app_storage_st{
	uint8	last_dimm_value[NUMBER_OF_LIGHT_PORTS];
	uint8	last_pds_value[NUMBER_OF_LIGHT_PORTS];
	uint32	last_rpt_interval;
#if defined (HAL_PDS) || defined (HAL_PDS_EXT)	
	uint32	zclOtoUDelay_32;
	uint8	zcldfltdemi;
#endif	
#if defined (HAL_RTC)
	struct 	dimmer_sched dim_sched [MAX_SCHEDULES];
	uint8	rst_idx;
	uint8 	cur_max_sched;
#endif
#if defined (HAL_RTC_RST)
	sched_state state;
	uint8	rtc_idx;
	uint8	rtc_tmp_idx;
	struct 	node_time last_rst_time;
#endif	
};
static struct app_storage_st app_storage;

#if defined (HAL_PDS)
static void zclQDimmer_HandlePDS( uint8 status );
#endif

#if defined (HAL_PDS_EXT)
uint8 zclQDimmer_ProcessInReportCmd( zclIncomingMsg_t *pInMsg );
#endif

#if defined (HAL_PDS) || defined (HAL_PDS_EXT)
static void occupancy_lighting_control(uint8 pds_no);
void prepare_send_select_epdata(uint8 ep_no);
#endif

#if defined (HAL_RTC)
void apply_fail_safe_sched(void);
void check_fail_safe_idx(void);
void adjust_fail_schedule_idx(void);
#endif

void UpdateDutyCycle_grad(uint8 dc, bool emer_flag, uint8 ctrl_no, uint8 old_dc);
void *App_ZdoJoinCnfCB ( void *pStr );
void initUart(halUARTCBack_t pf);
static void zclQDimmer_HandleKeys( byte shift, byte keys );
static void zclQDimmer_BasicResetCB( void );
static void zclQDimmer_IdentifyCB( zclIdentify_t *pCmd );
static void zclQDimmer_IdentifyQueryRspCB( zclIdentifyQueryRsp_t *pRsp );
static void zclQDimmer_ProcessIdentifyTimeChange( void );

// Functions to process ZCL Foundation incoming Command/Response messages
static void zclQDimmer_ProcessIncomingMsg( zclIncomingMsg_t *msg );
static uint8 zclQDimmer_ProcessInConfigReportCmd( zclIncomingMsg_t *pInMsg );
static uint8 zclSetAttrReportInterval( uint16 minReportInt );
#ifdef ZCL_READ
static uint8 zclQDimmer_ProcessInReadRspCmd( zclIncomingMsg_t *pInMsg );
#endif
#ifdef ZCL_WRITE
static uint8 zclQDimmer_ProcessInWriteRspCmd( zclIncomingMsg_t *pInMsg );
static uint8 zclQDimmer_ProcessInWriteCmd( zclIncomingMsg_t *pInMsg );
#endif
static uint8 zclQDimmer_ProcessInDefaultRspCmd( zclIncomingMsg_t *pInMsg );
#ifdef ZCL_DISCOVER
static uint8 zclQDimmer_ProcessInDiscRspCmd( zclIncomingMsg_t *pInMsg );
#endif
void initiate_binding (void);
void prepare_send_data(void);
void stop_flood(void);
void actual_current_consumption(void);
void clearnv_reinit(void);

/*********************************************************************
* ZCL General Profile Callback table
*/
static void zclQDimmer_LvlControlCB1 (zclLCMoveToLevel_t *pCmd);
static void zclQDimmer_LvlControlCBx (uint8 ep_no, uint8 dim_lvl);

#if defined (HAL_DIMMER_QUAD)
static void zclQDimmer_LvlControlCB2 (zclLCMoveToLevel_t *pCmd);
static void zclQDimmer_LvlControlCB3 (zclLCMoveToLevel_t *pCmd);
static void zclQDimmer_LvlControlCB4 (zclLCMoveToLevel_t *pCmd);
#endif


#if defined (ZCL_EZMODE)
uint8 zclQDimmerSeqNum;

static void zclQDimmer_ProcessZDOMsgs( zdoIncomingMsg_t *pMsg );
static void zclQDimmer_EZModeCB( zlcEZMode_State_t state, zclEZMode_CBData_t *pData );

static const zclEZMode_RegisterData_t zclQDimmer_RegisterEZModeData =
{
  &zclQDimmer_TaskID,
  QDIMMER_EZMODE_NEXTSTATE_EVT,
  QDIMMER_EZMODE_TIMEOUT_EVT,
  &zclQDimmerSeqNum,
  zclQDimmer_EZModeCB
};
#endif
devStates_t zclQDimmer_NwkState = DEV_INIT;

static zclGeneral_AppCallbacks_t zclQDimmer_CmdCallbacks_quad[] =
{
	{
		zclQDimmer_BasicResetCB,              // Basic Cluster Reset command
		zclQDimmer_IdentifyCB,                // Identify command
#ifdef ZCL_EZMODE
	    NULL, 								  // Identify EZ-Mode Invoke command
	    NULL, 								  // Identify Update Commission State command
#endif
		NULL,
		zclQDimmer_IdentifyQueryRspCB,        // Identify Query Response command
		NULL,                   			  // On/Off cluster command
		NULL,
		NULL,
		NULL,
		zclQDimmer_LvlControlCB1,              // Level Control Move to Level command
		NULL,                                     // Level Control Move command
		NULL,                                     // Level Control Step command
		NULL,                                     // Level Control Stop command
		#ifdef ZCL_GROUPS
		NULL,                                     // Group Response commands
		#endif
		#ifdef ZCL_SCENES
		NULL,                                     // Scene Store Request command
		NULL,                                     // Scene Recall Request command
		NULL,                                     // Scene Response command
		#endif
		#ifdef ZCL_ALARMS
		NULL,                                     // Alarm (Response) command
		#endif
		#ifdef SE_UK_EXT
		NULL,                                  // Get Event Log command
		NULL,                                  // Publish Event Log command
		#endif
		NULL,                                  // RSSI Location command
		NULL                                   // RSSI Location Response command
	},
#if defined (HAL_DIMMER_QUAD)
	{
		zclQDimmer_BasicResetCB,              // Basic Cluster Reset command
		zclQDimmer_IdentifyCB,                // Identify command
#ifdef ZCL_EZMODE
		NULL,								  // Identify EZ-Mode Invoke command
		NULL,								  // Identify Update Commission State command
#endif
		NULL,
		zclQDimmer_IdentifyQueryRspCB,        // Identify Query Response command
		NULL,                   // On/Off cluster command
		NULL,
		NULL,
		NULL,
		zclQDimmer_LvlControlCB2,              // Level Control Move to Level command
		NULL,                                     // Level Control Move command
		NULL,                                     // Level Control Step command
		NULL,                                     // Level Control Stop command
		#ifdef ZCL_GROUPS
		NULL,                                     // Group Response commands
		#endif
		#ifdef ZCL_SCENES
		NULL,                                     // Scene Store Request command
		NULL,                                     // Scene Recall Request command
		NULL,                                     // Scene Response command
		#endif
		#ifdef ZCL_ALARMS
		NULL,                                     // Alarm (Response) command
		#endif
		#ifdef SE_UK_EXT
		NULL,                                  // Get Event Log command
		NULL,                                  // Publish Event Log command
		#endif
		NULL,                                  // RSSI Location command
		NULL                                   // RSSI Location Response command
	},
	{
		zclQDimmer_BasicResetCB,              // Basic Cluster Reset command
		zclQDimmer_IdentifyCB,                // Identify command
#ifdef ZCL_EZMODE
		NULL,								  // Identify EZ-Mode Invoke command
		NULL,								  // Identify Update Commission State command
#endif
		NULL,
		zclQDimmer_IdentifyQueryRspCB,        // Identify Query Response command
		NULL,                   // On/Off cluster command
		NULL,
		NULL,
		NULL,
		zclQDimmer_LvlControlCB3,              // Level Control Move to Level command
		NULL,                                     // Level Control Move command
		NULL,                                     // Level Control Step command
		NULL,                                     // Level Control Stop command
		#ifdef ZCL_GROUPS
		NULL,                                     // Group Response commands
		#endif
		#ifdef ZCL_SCENES
		NULL,                                     // Scene Store Request command
		NULL,                                     // Scene Recall Request command
		NULL,                                     // Scene Response command
		#endif
		#ifdef ZCL_ALARMS
		NULL,                                     // Alarm (Response) command
		#endif
		#ifdef SE_UK_EXT
		NULL,                                  // Get Event Log command
		NULL,                                  // Publish Event Log command
		#endif
		NULL,                                  // RSSI Location command
		NULL                                   // RSSI Location Response command
	},
	{
		zclQDimmer_BasicResetCB,              // Basic Cluster Reset command
		zclQDimmer_IdentifyCB,                // Identify command
#ifdef ZCL_EZMODE
		NULL,								  // Identify EZ-Mode Invoke command
		NULL,								  // Identify Update Commission State command
#endif
		NULL,
		zclQDimmer_IdentifyQueryRspCB,        // Identify Query Response command
		NULL,                   // On/Off cluster command
		NULL,
		NULL,
		NULL,
		zclQDimmer_LvlControlCB4,              // Level Control Move to Level command
		NULL,                                     // Level Control Move command
		NULL,                                     // Level Control Step command
		NULL,                                     // Level Control Stop command
		#ifdef ZCL_GROUPS
		NULL,                                     // Group Response commands
		#endif
		#ifdef ZCL_SCENES
		NULL,                                     // Scene Store Request command
		NULL,                                     // Scene Recall Request command
		NULL,                                     // Scene Response command
		#endif
		#ifdef ZCL_ALARMS
		NULL,                                     // Alarm (Response) command
		#endif
		#ifdef SE_UK_EXT
		NULL,                                  // Get Event Log command
		NULL,                                  // Publish Event Log command
		#endif
		NULL,                                  // RSSI Location command
		NULL                                   // RSSI Location Response command
	},
#endif
};

/*********************************************************************
* @fn          zclQDimmer_Init
*
* @brief       Initialization function for the zclGeneral layer.
*
* @param       none
*
* @return      none
*/
void zclQDimmer_Init( byte task_id )
{
	zclQDimmer_TaskID = task_id;
	app_storage.last_rpt_interval = DEFAULT_REPORT_INTERVAL;

	zclQDimmer_DstAddr.addrMode = (afAddrMode_t)Addr16Bit;
	zclQDimmer_DstAddr.endPoint = 1;
	zclQDimmer_DstAddr.addr.shortAddr = 0;

	// Register the application's attribute list
	// This app is part of the Home Automation Profile

	for (uint8 i=0; i<NUMBER_OF_LIGHT_PORTS; i++)
	{
		zcl_DimmerDesc[i].EndPoint = DIMMER_ENDPOINT1 + i;
		zclHA_Init( &zcl_DimmerDesc[i] );
		zcl_registerAttrList( DIMMER_ENDPOINT1 + i, DIMMER_MAX_ATTRIBUTES, zclQDimmer_Attrs );
		// Register the ZCL General Cluster Library callback functions
		zclGeneral_RegisterCmdCallbacks( DIMMER_ENDPOINT1 + i, &zclQDimmer_CmdCallbacks_quad[i] );
		pDimmerReportCmd[i] = (zclReportCmd_t *)osal_mem_alloc( sizeof( zclReportCmd_t ) + ( numDimmerAttr * sizeof( zclReport_t ) ) );
		if ( pDimmerReportCmd != NULL )
		{
			pDimmerReportCmd[i]->numAttr = numDimmerAttr;
			
			pDimmerReportCmd[i]->attrList[0].attrID = ATTRID_WISYS_MGNT_VALUE;
			pDimmerReportCmd[i]->attrList[0].dataType = ZCL_DATATYPE_UINT40;
			pDimmerReportCmd[i]->attrList[0].attrData = &zclCounter_dimmer[i][0];
			
			pDimmerReportCmd[i]->attrList[1].attrID = ATTRID_WISYS_PWM_BASIC_PRESENT_VALUE;
			pDimmerReportCmd[i]->attrList[1].dataType = ZCL_DATATYPE_UINT16;
			pDimmerReportCmd[i]->attrList[1].attrData = &zclQDimmerData[i][0];

#if defined (HAL_PDS) || defined (HAL_PDS_EXT)
			pDimmerReportCmd[i]->attrList[2].attrID = ATTRID_WISYS_MS_PDS_MEASURED_VALUE;
			pDimmerReportCmd[i]->attrList[2].dataType = ZCL_DATATYPE_UINT16;
			pDimmerReportCmd[i]->attrList[2].attrData = &zclPdsData[i][0];
#endif
		}
	}

	ZMacSetTransmitPower(TX_PWR_PLUS_19);


	// Register the Application to receive the unprocessed Foundation command/response messages
	zcl_registerForMsg( zclQDimmer_TaskID );

	// Register for all key events - This app will handle all key events
	RegisterForKeys( zclQDimmer_TaskID );

	/* Register for App to receive Join Confirm */
	ZDO_RegisterForZdoCB( ZDO_JOIN_CNF_CBID, &App_ZdoJoinCnfCB );

	uint8 nv_sts = osal_nv_item_init( NV_APP_DATA, sizeof(struct app_storage_st), &app_storage );
	if (  nv_sts == ZSUCCESS )
	{
		// Get the old value from NV memory
		osal_nv_read( NV_APP_DATA, 0, sizeof(app_storage), &app_storage );
	}

#if defined (HAL_PDS) || defined (HAL_PDS_EXT)
	zcldfltdemi = app_storage.zcldfltdemi;
	zclOtoUDelay_32 = app_storage.zclOtoUDelay_32;
#endif

#if defined (HAL_PDS)
	RegisterForpds( zclQDimmer_TaskID );
	pds_init(zclQDimmer_TaskID, QUAD_ENABLE);
#endif        	

	InitPWM(QUAD_ENABLE);
	zclQDimmer_OnOff = LIGHT_OFF;

#if (defined (HAL_DIMMER) || defined (HAL_DIMMER_QUAD)) &&(defined (HAL_PDS) || defined (HAL_PDS_EXT))
	for (uint8 i=1; i<NUMBER_OF_LIGHT_PORTS+1; i++){
		UpdateDutyCycle(app_storage.zcldfltdemi, EMERGENCY_OFF, i-1);  // Start PWM
		zclQDimmerData[i-1][1] = (app_storage.zcldfltdemi >> 8)&0xFF;
		zclQDimmerData[i-1][0] = (app_storage.zcldfltdemi)&0xFF;
	}        
#else
	for (uint8 i=1; i<NUMBER_OF_LIGHT_PORTS+1; i++){
		UpdateDutyCycle(app_storage.last_dimm_value[i-1], EMERGENCY_OFF, i-1);  // Start PWM
		zclQDimmerData[i-1][1] = (app_storage.last_dimm_value[i-1] >> 8)&0xFF;
		zclQDimmerData[i-1][0] = (app_storage.last_dimm_value[i-1])&0xFF;
	}
#endif  

#if defined (HAL_RTC)
	DS1307_Init();
#endif
#if defined (HAL_RTC_RST)
	if (!(DS1307_GetTime(&hh,&mm,&ss)))
	{
		if (nv_sts == NV_ITEM_UNINIT){
#if DEBUG
			DS1307_SetTime(0x15, 0x15, 0x00);
			DS1307_GetTime(&hh,&mm,&ss);
#endif
			app_storage.last_rst_time.hh = hh;
			app_storage.last_rst_time.mm = mm;
			app_storage.last_rst_time.ss = ss;
			app_storage.state = init;
			app_storage.rtc_idx = 0x0;
		}
	}	
#endif

#if defined (HAL_RTC_RST) && defined ( NV_RESTORE )
	check_inside_sm();		
#endif	//End of remote RST

#if defined (HAL_RTC)
    adjust_fail_schedule_idx();
#endif

osal_start_timerEx( zclQDimmer_TaskID, SOLARMON_REPORT_EVT, osal_rand());
	newminReportInt =  app_storage.last_rpt_interval;		//app_storage.last_rpt_interval;

#ifdef ZCL_EZMODE
  // Register EZ-Mode
  zcl_RegisterEZMode( &zclQDimmer_RegisterEZModeData );
  // Register with the ZDO to receive Match Descriptor Responses
  ZDO_RegisterForZDOMsg(task_id, Match_Desc_rsp);
#endif
}

/***************************************************************************************************
* @fn          MT_ZdoJoinCnfCB
*
* @brief       Handle the ZDO Join Confirm from ZDO
*
* @param       pStr - pointer to a parameter and a structure of parameters
*
* @return      void
***************************************************************************************************/
void *App_ZdoJoinCnfCB ( void *pStr )
{
	return NULL;
}

/*********************************************************************
* @fn          zclSample_event_loop
*
* @brief       Event Loop Processor for zclGeneral.
*
* @param       none
*
* @return      none
*/
uint16 zclQDimmer_event_loop( uint8 task_id, uint16 events )
{
	afIncomingMSGPacket_t *MSGpkt; //static int counter=0;
	(void)task_id;  // Intentionally unreferenced parameter
	
	if ( events & SYS_EVENT_MSG )
	{
		while ( (MSGpkt = (afIncomingMSGPacket_t *)osal_msg_receive( zclQDimmer_TaskID )) )
		{
			switch ( MSGpkt->hdr.event )
			{
			case ZCL_INCOMING_MSG:
				// Incoming ZCL Foundation command/response messages
				zclQDimmer_ProcessIncomingMsg( (zclIncomingMsg_t *)MSGpkt );
				break;

			case ZDO_CB_MSG:
#ifdef ZCL_EZMODE                          
				zclQDimmer_ProcessZDOMsgs( (zdoIncomingMsg_t *)MSGpkt );
#endif                                
				break;

			case KEY_CHANGE:
				zclQDimmer_HandleKeys( ((keyChange_t *)MSGpkt)->state, ((keyChange_t *)MSGpkt)->keys );
				break;

#if defined (HAL_PDS)
			case HAL_PDS_EVENT:
				zclQDimmer_HandlePDS(((keyChange_t *)MSGpkt)->state);
				break;
#endif
			case ZDO_NWK_JOIN_REQ:
				break;
				
			case ZDO_STATE_CHANGE:
			  	zclQDimmer_NwkState = (devStates_t)(MSGpkt->hdr.status);
				break;
				
			default:
				break;
			}

			// Release the memory
			osal_msg_deallocate( (uint8 *)MSGpkt );
		}

		// return unprocessed events
		return (events ^ SYS_EVENT_MSG);
	}

	if ( events & SAMPLELIGHT_IDENTIFY_TIMEOUT_EVT )
	{
		if ( zclQDimmer_IdentifyTime > 0 )
		zclQDimmer_IdentifyTime--;
		zclQDimmer_ProcessIdentifyTimeChange();

		return ( events ^ SAMPLELIGHT_IDENTIFY_TIMEOUT_EVT );
	}

	if ( events & SOLARMON_REPORT_EVT )
	{
		prepare_send_data();
		return ( events ^ SOLARMON_REPORT_EVT );
	}

#if defined (HAL_PDS)

	uint8 pds_no=0;
	if ( events & PDS_REPORT_EVT0){
		pds_no = 0;
		occupancy_lighting_control(pds_no);
		osal_start_timerEx( zclQDimmer_TaskID, PDS_REPORT_EVT0, PDS_FIRST_INTERVAL);
		return ( events ^ PDS_REPORT_EVT0 );
	}

	if ( events & PDS_REPORT_EVT1){
		pds_no = 1;
		occupancy_lighting_control(pds_no);
		osal_start_timerEx( zclQDimmer_TaskID, PDS_REPORT_EVT1, PDS_FIRST_INTERVAL);
		return ( events ^ PDS_REPORT_EVT1 );
	}
	if ( events & PDS_REPORT_EVT2){
		pds_no = 2;
		occupancy_lighting_control(pds_no);
		osal_start_timerEx( zclQDimmer_TaskID, PDS_REPORT_EVT2, PDS_FIRST_INTERVAL);
		return ( events ^ PDS_REPORT_EVT2 );
	}
	if ( events & PDS_REPORT_EVT3){
		pds_no = 3;
		occupancy_lighting_control(pds_no);
		osal_start_timerEx( zclQDimmer_TaskID, PDS_REPORT_EVT3, PDS_FIRST_INTERVAL);
		return ( events ^ PDS_REPORT_EVT3 );
	}

#endif
	// Discard unknown events
#ifdef ZCL_EZMODE
	  if ( events & PDS_REPORT_EVT){
		  occupancy_lighting_control(0);
		  osal_start_timerEx( zclQDimmer_TaskID, PDS_REPORT_EVT, PDS_FIRST_INTERVAL);
		  return ( events ^ PDS_REPORT_EVT );
	  }

	  // going on to next state
	  if ( events & QDIMMER_EZMODE_NEXTSTATE_EVT )
	  {
		zcl_EZModeAction ( EZMODE_ACTION_PROCESS, NULL );	// going on to next state
		return ( events ^ QDIMMER_EZMODE_NEXTSTATE_EVT );
	  }
	
	  // the overall EZMode timer expired, so we timed out
	  if ( events & QDIMMER_EZMODE_TIMEOUT_EVT )
	  {
		zcl_EZModeAction ( EZMODE_ACTION_TIMED_OUT, NULL ); // EZ-Mode timed out
		return ( events ^ QDIMMER_EZMODE_TIMEOUT_EVT );
	  }
#endif // ZLC_EZMODE
	
	return 0;
}

#if defined (HAL_PDS) || defined (HAL_PDS_EXT)
static void occupancy_lighting_control(uint8 pds_no)
{
	uint8  dimm_value = 0;

	dimm_value |= zclQDimmerData[pds_no][0];

	if ((pds_last_state[pds_no] == UNOCCUPIED) && (dimm_value != zcldfltdemi))
	{
		if (++otoucounts[pds_no] == zclOtoUDelay_32/PDS_FIRST_INTERVAL)
		{
			otoucounts[pds_no] = 0;
			if (occupancy_override_flag == TRUE)
			{
				occupancy_override_flag = FALSE;
				UpdateDutyCycle_grad(app_storage.last_dimm_value[0], EMERGENCY_OFF, pds_no ,dimm_value);
				zclQDimmerData[pds_no][0] = (app_storage.last_dimm_value[pds_no]);
			}
			else {
				UpdateDutyCycle_grad(zcldfltdemi, EMERGENCY_OFF, pds_no , dimm_value);
				zclQDimmerData[pds_no][0] = (zcldfltdemi);
			}	
			prepare_send_select_epdata(pds_no);
			osal_start_timerEx( zclQDimmer_TaskID, SOLARMON_REPORT_EVT, newminReportInt+osal_rand());
			
		}
		//This should never happen, it is an error condition, reset it.
		else if (otoucounts[pds_no] > zclOtoUDelay_32/PDS_FIRST_INTERVAL)
		{
			otoucounts[pds_no] = 0;
		
		}
	}
	if (pds_triggered[pds_no] != pds_last_state[pds_no])
	{
		otoucounts[pds_no] = 0;
		pds_last_state[pds_no] = pds_triggered[pds_no];
		if (pds_last_state[pds_no] == OCCUPIED)
		{
			UpdateDutyCycle_grad(app_storage.last_dimm_value[pds_no], EMERGENCY_OFF, pds_no , dimm_value);
			//      app_storage.last_dimm_value[0]=DEFAULT_DIMMING_FULL;
			zclQDimmerData[pds_no][0] = (app_storage.last_dimm_value[pds_no]);
		}
		app_storage.last_pds_value[pds_no] = pds_triggered[pds_no];
		prepare_send_select_epdata(pds_no);
		osal_start_timerEx( zclQDimmer_TaskID, SOLARMON_REPORT_EVT, newminReportInt+osal_rand());
	}
#if defined (HAL_PDS)
// Do not do this if PDS is external
	pds_triggered[pds_no] = UNOCCUPIED;
#endif
}
#endif

#if defined (HAL_PDS)
void zclQDimmer_HandlePDS( uint8 pds_no )
{
	if(pds_triggered[pds_no] == OCCUPIED)
	osal_start_timerEx( zclQDimmer_TaskID, PDS_REPORT_EVT << pds_no, PDS_OCCUPANCY_INTERVAL);
	pds_triggered[pds_no] = OCCUPIED;
}
#endif

#if defined (HAL_PDS) || defined (HAL_PDS_EXT)
static void prepare_send_select_epdata(uint8 ep_no){
    uint16 rand = 0;

	zclPdsData[ep_no][1] = (app_storage.last_pds_value[ep_no] >> 8)&0xFF;
	zclPdsData[ep_no][0] = (app_storage.last_pds_value[ep_no])&0xFF;

	actual_current_consumption();
    rand = ((osal_rand()% 1000)/3); //Give some delay in case there is back to back data from all nodes
    DelayMilliSeconds(rand);
	zclCounter_dimmer[ep_no][3] = LO_UINT16( parent_addr );
	zclCounter_dimmer[ep_no][4] = HI_UINT16( parent_addr );

	zcl_SendReportCmd( DIMMER_ENDPOINT1 + ep_no, &zclQDimmer_DstAddr,
	ZCL_CLUSTER_ID_WISYS_CLUSTER, pDimmerReportCmd[ep_no],
	ZCL_FRAME_SERVER_CLIENT_DIR, 0, 0 );
}
#endif

static void prepare_send_select_epdata_dummy(uint8 ep_no){
	actual_current_consumption();
//	zclCounter_dimmer[ep_no][3] = LO_UINT16( parent_addr ); rtc test JK mm change during schedule
	zclCounter_dimmer[ep_no][4] = HI_UINT16( parent_addr );
	zcl_SendReportCmd( DIMMER_ENDPOINT1 + ep_no, &zclQDimmer_DstAddr,
	ZCL_CLUSTER_ID_WISYS_CLUSTER, pDimmerReportCmd[ep_no],
	ZCL_FRAME_SERVER_CLIENT_DIR, 0, 0 );
}

static void prepare_send_data(void)
{
	static uint8 counter=0;
	
	static uint16 counter_dimmer=0;

	actual_current_consumption();

#if defined (HAL_PDS) || defined (HAL_PDS_EXT)
	zclPdsData[counter][1] = (app_storage.last_pds_value[counter] >> 8)&0xFF;
	zclPdsData[counter][0] = (app_storage.last_pds_value[counter])&0xFF;
#endif
	counter_dimmer++;
	zclCounter_dimmer[counter][0] = (counter_dimmer)&0xFF;
	zclCounter_dimmer[counter][1] = (counter_dimmer >> 8)&0xFF;
	zclCounter_dimmer[counter][4] = HI_UINT16( parent_addr );
	zcl_SendReportCmd( DIMMER_ENDPOINT1 + counter, &zclQDimmer_DstAddr,
	ZCL_CLUSTER_ID_WISYS_CLUSTER, pDimmerReportCmd[counter],
	ZCL_FRAME_SERVER_CLIENT_DIR, 0, 0 );
#if defined (HAL_RTC)
	data_sent_flag = TRUE; 	//turn off fail safe mode- we are on network
	check_fail_safe_idx();
#endif
	if (++counter == NUMBER_OF_LIGHT_PORTS){
		counter = 0;	//Reset the counter
		osal_start_timerEx( zclQDimmer_TaskID, SOLARMON_REPORT_EVT, newminReportInt);
	}
	else{
		osal_start_timerEx( zclQDimmer_TaskID, SOLARMON_REPORT_EVT, FLOOD_TIMING);
	}
	
	if (flood_active && (flood_fs_cntr++ == MAX_FLOOD_COUNT))
	stop_flood();

}
void actual_current_consumption(void)
{      
        uint8 counter=0;
#if defined (HAL_DIMMER_ADC)
        uint16 temp16=0;
        HalAdcSetReference(HAL_ADC_REF_125V);
        for (counter=0; counter<NUMBER_OF_LIGHT_PORTS; counter++)
		{   
			
			switch(counter)
			{
			case 0:
				temp16 = ((HalAdcRead(HAL_ADC_CHANNEL_0, HAL_ADC_RESOLUTION_10)));
				break;
			case 1:
				temp16 = ((HalAdcRead(HAL_ADC_CHANNEL_1, HAL_ADC_RESOLUTION_10)));
				break;
			case 2:
				temp16 = ((HalAdcRead(HAL_ADC_CHANNEL_2, HAL_ADC_RESOLUTION_10)));
				break;
			case 3:
				temp16 = ((HalAdcRead(HAL_ADC_CHANNEL_7, HAL_ADC_RESOLUTION_10)));
				break;       
			}
			temp16 = (temp16*100)/0x3FF;
			zclQDimmerData[counter][1] = temp16 & 0xFF;
		}
#else 
       for (counter=0; counter<NUMBER_OF_LIGHT_PORTS; counter++)  
         zclQDimmerData[counter][1] = zclQDimmerData[counter][0];
#endif
}
/*********************************************************************
* @fn      zclSolarMon_HandleKeys
*
* @brief   Handles all key events for this device.
*
* @param   shift - true if in shift/alt.
* @param   keys - bit field for key events. Valid entries:
*                 HAL_KEY_SW_4
*                 HAL_KEY_SW_3
*                 HAL_KEY_SW_2
*                 HAL_KEY_SW_1
*
* @return  none
*/
void zclQDimmer_HandleKeys( byte shift, byte keys )
{
	//  zAddrType_t dstAddr;

	(void)shift;  // Intentionally unreferenced parameter
#if !defined (HAL_CUSTOM_BOARD)
	if ( keys & HAL_KEY_SW_1 )
#else
	if ( keys & HAL_KEY_EXTSW_1 )
#endif
	
	{
		//    HalLedBlink ( HAL_LED_1, 0, 50, HAL_LED_DEFAULT_FLASH_TIME );
	}

	if ( keys & HAL_KEY_EXTSW_2 )
	{
#ifdef ZCL_EZMODE
			// Invoke EZ-Mode
		zclEZMode_InvokeData_t ezModeData;

		// Invoke EZ-Mode
		ezModeData.endpoint = DIMMER_ENDPOINT1; // endpoint on which to invoke EZ-Mode
		if ( ( zclQDimmer_NwkState == DEV_ZB_COORD ) ||
			 ( zclQDimmer_NwkState == DEV_ROUTER )	 ||
			 ( zclQDimmer_NwkState == DEV_END_DEVICE ) )
		{
		  ezModeData.onNetwork = TRUE;		// node is already on the network
		}
		else
		{
		  ezModeData.onNetwork = FALSE; 	// node is not yet on the network
		}
		ezModeData.initiator = FALSE;		 // DoorLock Device is a target
		zcl_InvokeEZMode( &ezModeData );
	
 #ifdef LCD_SUPPORTED
		HalLcdWriteString( "EZMode", HAL_LCD_LINE_2 );
 #endif
#endif 
	}

	if ( keys & HAL_KEY_SW_3 )
	{
	}

	if ( keys & HAL_KEY_SW_4 )
	{
    }
}

/*********************************************************************
* @fn      zclDimmer_ProcessIdentifyTimeChange
*
* @brief   Called to process any change to the IdentifyTime attribute.
*
* @param   none
*
* @return  none
*/
static void zclQDimmer_ProcessIdentifyTimeChange( void )
{
	if ( zclQDimmer_IdentifyTime > 0 )
	{
		osal_start_timerEx( zclQDimmer_TaskID, SAMPLELIGHT_IDENTIFY_TIMEOUT_EVT, 1000 );
		HalLedBlink ( HAL_LED_1, 0xFF, HAL_LED_DEFAULT_DUTY_CYCLE, HAL_LED_DEFAULT_FLASH_TIME );
	}
	else
	{
		if ( zclQDimmer_OnOff )
		HalLedSet ( HAL_LED_1, HAL_LED_MODE_ON );
		else
		HalLedSet ( HAL_LED_1, HAL_LED_MODE_OFF );
		osal_stop_timerEx( zclQDimmer_TaskID, SAMPLELIGHT_IDENTIFY_TIMEOUT_EVT );
	}
}

/*********************************************************************
* @fn      zclQDimmer_BasicResetCB
*
* @brief   Callback from the ZCL General Cluster Library
*          to set all the Basic Cluster attributes to default values.
*
* @param   none
*
* @return  none
*/
static void zclQDimmer_BasicResetCB( void )
{
	// Reset all attributes to default values
}

/*********************************************************************
* @fn      zclQDimmer_IdentifyCB
*
* @brief   Callback from the ZCL General Cluster Library when
*          it received an Identity Command for this application.
*
* @param   srcAddr - source address and endpoint of the response message
* @param   identifyTime - the number of seconds to identify yourself
*
* @return  none
*/
static void zclQDimmer_IdentifyCB( zclIdentify_t *pCmd )
{
	zclQDimmer_IdentifyTime = pCmd->identifyTime;
	zclQDimmer_ProcessIdentifyTimeChange();
}

/*********************************************************************
* @fn      zclQDimmer_IdentifyQueryRspCB
*
* @brief   Callback from the ZCL General Cluster Library when
*          it received an Identity Query Response Command for this application.
*
* @param   srcAddr - requestor's address
* @param   timeout - number of seconds to identify yourself (valid for query response)
*
* @return  none
*/
static void zclQDimmer_IdentifyQueryRspCB(  zclIdentifyQueryRsp_t *pRsp )
{
	// Query Response (with timeout value)
	(void)pRsp;
#ifdef ZCL_EZMODE
	  {
		zclEZMode_ActionData_t data;
		data.pIdentifyQueryRsp = pRsp;
		zcl_EZModeAction ( EZMODE_ACTION_IDENTIFY_QUERY_RSP, &data );
	  }
#endif

}

static void zclQDimmer_LvlControlCB1 (zclLCMoveToLevel_t *pCmd){
	zclQDimmer_LvlControlCBx(DIMMER_ENDPOINT1, pCmd->level);
	prepare_send_select_epdata_dummy(0);
}

#if defined (HAL_DIMMER_QUAD)
static void zclQDimmer_LvlControlCB2 (zclLCMoveToLevel_t *pCmd){
	zclQDimmer_LvlControlCBx(DIMMER_ENDPOINT2, pCmd->level);
	prepare_send_select_epdata_dummy(1);
}
static void zclQDimmer_LvlControlCB3 (zclLCMoveToLevel_t *pCmd){
	zclQDimmer_LvlControlCBx(DIMMER_ENDPOINT3, pCmd->level);
	prepare_send_select_epdata_dummy(2);
}
static void zclQDimmer_LvlControlCB4 (zclLCMoveToLevel_t *pCmd){
	zclQDimmer_LvlControlCBx(DIMMER_ENDPOINT4, pCmd->level);
	prepare_send_select_epdata_dummy(3);
}
#endif

/*********************************************************************
* @fn      zclQDimmer_OnOffCB
*
* @brief   Callback from the ZCL General Cluster Library when
*          it received an On/Off Command for this application.
*
* @param   cmd - COMMAND_ON, COMMAND_OFF or COMMAND_TOGGLE
*
* @return  none
*/
static void zclQDimmer_LvlControlCBx (uint8 ep_no, uint8 dim_lvl){
#if defined (HAL_PDS)
	if ((pds_last_state[ep_no-DIMMER_ENDPOINT1] == OCCUPIED) && (dim_lvl == 0)){
		occupancy_override_flag = TRUE;
		app_storage.last_dimm_value[ep_no-DIMMER_ENDPOINT1] = dim_lvl;	
		osal_nv_write(NV_APP_DATA, 0, sizeof(struct app_storage_st), &app_storage);
		return;
	}
#endif	
	zclQDimmerData[ep_no-DIMMER_ENDPOINT1][1] = (dim_lvl >> 8)&0xFF;
	zclQDimmerData[ep_no-DIMMER_ENDPOINT1][0] = (dim_lvl)&0xFF;

	if (dim_lvl == 101)
		UpdateDutyCycle(50, EMERGENCY_ON, ep_no-DIMMER_ENDPOINT1);
	else
		UpdateDutyCycle_grad(dim_lvl, EMERGENCY_OFF, ep_no-DIMMER_ENDPOINT1, app_storage.last_dimm_value[ep_no-DIMMER_ENDPOINT1]);

	app_storage.last_dimm_value[ep_no-DIMMER_ENDPOINT1] = dim_lvl;
	osal_nv_write(NV_APP_DATA, 0, sizeof(struct app_storage_st), &app_storage);

	return;	
}

/******************************************************************************
*
*  Functions for processing ZCL Foundation incoming Command/Response messages
*
*****************************************************************************/

/*********************************************************************
* @fn      zclQDimmer_ProcessIncomingMsg
*
* @brief   Process ZCL Foundation incoming message
*
* @param   pInMsg - pointer to the received message
*
* @return  none
*/
static void zclQDimmer_ProcessIncomingMsg( zclIncomingMsg_t *pInMsg)
{
	switch ( pInMsg->zclHdr.commandID )
	{
#ifdef ZCL_READ
	case ZCL_CMD_READ_RSP:
		zclQDimmer_ProcessInReadRspCmd( pInMsg );
		break;
#endif
#ifdef ZCL_WRITE
	case ZCL_CMD_WRITE_RSP:
		zclQDimmer_ProcessInWriteRspCmd( pInMsg );
		break;
		
	case ZCL_CMD_WRITE:
		zclQDimmer_ProcessInWriteCmd( pInMsg );
		break;
		
#endif
#ifdef ZCL_REPORT
		// See ZCL Test Applicaiton (zcl_testapp.c) for sample code on Attribute Reporting
	case ZCL_CMD_CONFIG_REPORT:
		zclQDimmer_ProcessInConfigReportCmd( pInMsg );
		break;
		
	case ZCL_CMD_CONFIG_REPORT_RSP:
		//zclQDimmer_ProcessInConfigReportRspCmd( pInMsg );
		break;
		
	case ZCL_CMD_READ_REPORT_CFG:
		//zclQDimmer_ProcessInReadReportCfgCmd( pInMsg );
		break;
		
	case ZCL_CMD_READ_REPORT_CFG_RSP:
		//zclQDimmer_ProcessInReadReportCfgRspCmd( pInMsg );
		break;
		
	case ZCL_CMD_REPORT:
#ifdef ZCL_EZMODE          
		zclQDimmer_ProcessInReportCmd( pInMsg );
#endif
		break;
#endif
	case ZCL_CMD_DEFAULT_RSP:
		zclQDimmer_ProcessInDefaultRspCmd( pInMsg );
		break;
#ifdef ZCL_DISCOVER
	case ZCL_CMD_DISCOVER_RSP:
		zclQDimmer_ProcessInDiscRspCmd( pInMsg );
		break;
#endif
	default:
		break;
	}

	if ( pInMsg->attrCmd )
	osal_mem_free( pInMsg->attrCmd );
}

#ifdef ZCL_READ
/*********************************************************************
* @fn      zclQDimmer_ProcessInReadRspCmd
*
* @brief   Process the "Profile" Read Response Command
*
* @param   pInMsg - incoming message to process
*
* @return  none
*/
static uint8 zclQDimmer_ProcessInReadRspCmd( zclIncomingMsg_t *pInMsg )
{
	zclReadRspCmd_t *readRspCmd;
	uint8 i;

	readRspCmd = (zclReadRspCmd_t *)pInMsg->attrCmd;
	for (i = 0; i < readRspCmd->numAttr; i++)
	{
		// Notify the originator of the results of the original read attributes
		// attempt and, for each successfull request, the value of the requested
		// attribute
	}

	return TRUE;
}
#endif // ZCL_READ

#ifdef ZCL_WRITE
/*********************************************************************
* @fn      zclQDimmer_ProcessInWriteRspCmd
*
* @brief   Process the "Profile" Write Response Command
*
* @param   pInMsg - incoming message to process
*
* @return  none
*/
static uint8 zclQDimmer_ProcessInWriteRspCmd( zclIncomingMsg_t *pInMsg )
{
	zclWriteRspCmd_t *writeRspCmd;
	uint8 i;

	writeRspCmd = (zclWriteRspCmd_t *)pInMsg->attrCmd;
	for (i = 0; i < writeRspCmd->numAttr; i++)
	{
		// Notify the device of the results of the its original write attributes
		// command.
	}

	return TRUE;
}

/*********************************************************************
* @fn      zclQDimmer_ProcessInWriteRspCmd
*
* @brief   Process the "Profile" Write Response Command
*
* @param   pInMsg - incoming message to process
*
* @return  none
*/
static uint8 zclQDimmer_ProcessInWriteCmd( zclIncomingMsg_t *pInMsg )
{
	zclWriteCmd_t *writeCmd; zclWriteRspCmd_t *writerspCmd=NULL;
	uint8 i;
#if defined (HAL_PDS) || defined (HAL_PDS_EXT)        
        uint8 j;
#endif        

#if defined (HAL_RTC)
	uint8 rtc_payload_len=0;
#endif

	//RTC Packet: PACK_LEN, RTC_HH, RTC_MM, XX, XX, RTC_FIRST_SCHED...

#define RTC_FIRST_SCHED_OFF		0x5
#define RTC_HH_OFFSET			0x1
#define RTC_MM_OFFSET			0x2

	writeCmd = (zclWriteCmd_t *)pInMsg->attrCmd;

	// Allocate space for the response command
	writerspCmd = (zclWriteRspCmd_t *)osal_mem_alloc( sizeof ( zclWriteRspCmd_t ) + \
	sizeof ( zclWriteRspStatus_t) * writeCmd->numAttr );
	if ( writerspCmd == NULL )
	return FALSE; // EMBEDDED RETURN

	for (i = 0; i < writeCmd->numAttr; i++)
	{
		if(writeCmd->attrList[0].attrID == ATTRID_WISYS_OCCUPANCY_SENSING_CONFIG_PIR_O_TO_U_DELAY){
#if defined (HAL_PDS) || defined (HAL_PDS_EXT)
			zclOtoUDelay = 0;
			zclOtoUDelay |= writeCmd->attrList[0].attrData[0];
			zclOtoUDelay |= writeCmd->attrList[0].attrData[1] << 8;
			zclOtoUDelay_32 = zclOtoUDelay;
			zclOtoUDelay_32 = (zclOtoUDelay_32*1000);
			for (j=0; j<NUMBER_OF_LIGHT_PORTS; j++)
				otoucounts[j] = 0;
			app_storage.zclOtoUDelay_32 = zclOtoUDelay_32;
			osal_nv_write(NV_APP_DATA, 0, sizeof(struct app_storage_st), &app_storage);
			// Notify the device of the results of the its original write attributes
			// command.
#endif
		}
		else if(writeCmd->attrList[0].attrID == ATTRID_WISYS_OCCUPANCY_SENSING_DEFAULT_DIMMING_LEVEL){
#if defined (HAL_PDS) || defined (HAL_PDS_EXT)
			zcldfltdemi= writeCmd->attrList[0].attrData[0];
			app_storage.zcldfltdemi = zcldfltdemi;
			osal_nv_write(NV_APP_DATA, 0, sizeof(struct app_storage_st), &app_storage);
			
#endif
		}

		else if(writeCmd->attrList[0].attrID == ATTRID_WISYS_SCHED_TABLE){
#if defined (HAL_RTC)
			rtc_payload_len =  writeCmd->attrList[0].attrData[0]-5;

			if ( rtc_payload_len > sizeof(struct dimmer_sched) * MAX_SCHEDULES || (rtc_payload_len % sizeof (struct dimmer_sched)))	//Ignore total byte offset
				return FALSE;
			app_storage.cur_max_sched =  rtc_payload_len/sizeof (struct dimmer_sched);				
			osal_memcpy(app_storage.dim_sched, writeCmd->attrList[0].attrData+RTC_FIRST_SCHED_OFF, writeCmd->attrList[0].attrData[0]-5); //First byte is size always + skip 4 bytes from time
			DS1307_SetTime(*(writeCmd->attrList[0].attrData+RTC_HH_OFFSET),*(writeCmd->attrList[0].attrData+RTC_MM_OFFSET), 0);	//Sync the RTC time			
            adjust_fail_schedule_idx();
			osal_nv_write(NV_APP_DATA, 0, sizeof(struct app_storage_st), &app_storage);
#endif
		}
	}

	// Since all attributes were configured successfully, include a single
	// attribute status record in the response command with the status field
	// set to SUCCESS and the attribute ID field omitted.
	writerspCmd->attrList[0].status = ZCL_STATUS_SUCCESS;
	writerspCmd->numAttr = 1;
	
	zcl_SendWriteRsp( pInMsg->endPoint, &(pInMsg->srcAddr),
	pInMsg->clusterId, writerspCmd, ZCL_FRAME_SERVER_CLIENT_DIR,
	true, pInMsg->zclHdr.transSeqNum );

	return TRUE;
}
#endif // ZCL_WRITE
/*********************************************************************
* @fn      zclQDimmer_ProcessInDefaultRspCmd
*
* @brief   Process the "Profile" Default Response Command
*
* @param   pInMsg - incoming message to process
*
* @return  none
*/
static uint8 zclQDimmer_ProcessInDefaultRspCmd( zclIncomingMsg_t *pInMsg )
{
	// zclDefaultRspCmd_t *defaultRspCmd = (zclDefaultRspCmd_t *)pInMsg->attrCmd;
	// Device is notified of the Default Response command.
#if defined (HAL_RTC)
	data_sent_flag = FALSE; 	//turn off fail safe mode- we are on network
	fail_safe_mode = FALSE;
	fs_cntr = 0;
#endif

#if defined (HAL_RTC_RST)
	if (!app_storage.rtc_idx){
		data_sent_flag = TRUE; 	//Override and fake that i could not send data so we enter fail safe
		fail_safe_mode = TRUE;
	}
#endif	

	//	(void)pInMsg;
	return TRUE;
}

#ifdef ZCL_DISCOVER
/*********************************************************************
* @fn      zclQDimmer_ProcessInDiscRspCmd
*
* @brief   Process the "Profile" Discover Response Command
*
* @param   pInMsg - incoming message to process
*
* @return  none
*/
static uint8 zclQDimmer_ProcessInDiscRspCmd( zclIncomingMsg_t *pInMsg )
{
	zclDiscoverRspCmd_t *discoverRspCmd;
	uint8 i;

	discoverRspCmd = (zclDiscoverRspCmd_t *)pInMsg->attrCmd;
	for ( i = 0; i < discoverRspCmd->numAttr; i++ )
	{
		// Device is notified of the result of its attribute discovery command.
	}

	return TRUE;
}
#endif // ZCL_DISCOVER


#if defined ( ZCL_REPORT )
/*********************************************************************
* @fn      simplemeter_ProcessInConfigReportCmd
*
* @brief   Process the "Profile" Configure Reporting Command
*
* @param   pInMsg - incoming message to process
*
* @return  TRUE if attribute was found in the Attribute list,
*          FALSE if not
*/
static uint8 zclQDimmer_ProcessInConfigReportCmd( zclIncomingMsg_t *pInMsg )
{
	zclCfgReportCmd_t *cfgReportCmd;
	zclCfgReportRec_t *reportRec;
	zclCfgReportRspCmd_t *cfgReportRspCmd;
	zclAttrRec_t attrRec;
	uint8 status;
	uint8 i, j = 0;

	cfgReportCmd = (zclCfgReportCmd_t *)pInMsg->attrCmd;

	// Allocate space for the response command
	cfgReportRspCmd = (zclCfgReportRspCmd_t *)osal_mem_alloc( sizeof ( zclCfgReportRspCmd_t ) + \
	sizeof ( zclCfgReportStatus_t) * cfgReportCmd->numAttr );
	if ( cfgReportRspCmd == NULL )
	return FALSE; // EMBEDDED RETURN

	// Process each Attribute Reporting Configuration record
	for ( i = 0; i < cfgReportCmd->numAttr; i++ )
	{
		reportRec = &(cfgReportCmd->attrList[i]);

		status = ZCL_STATUS_SUCCESS;

		if (1)              //Assume right attribute
		{
			if ( reportRec->direction == ZCL_SEND_ATTR_REPORTS )
			{
				//        if ( reportRec->dataType == attrRec.attr.dataType )
				//        {
				// This the attribute that is to be reported
				if ( zcl_MandatoryReportableAttribute( &attrRec ) == TRUE )
				{
					// Set the Min and Max Reporting Intervals and Reportable Change
					if(reportRec->minReportInt == 1)
						initiate_binding();
					else if(reportRec->minReportInt)
						status = zclSetAttrReportInterval( reportRec->minReportInt );
					else
						clearnv_reinit();

					status = ZCL_STATUS_SUCCESS; // for now
				}
				else
				{
					// Attribute cannot be reported
					status = ZCL_STATUS_UNREPORTABLE_ATTRIBUTE;
				}
			}
			//        else
			//        {
			// Attribute data type is incorrect
			//          status = ZCL_STATUS_INVALID_DATA_TYPE;
			//        }
			//      }
			else
			{
				// We shall expect reports of values of this attribute
				if ( zcl_MandatoryReportableAttribute( &attrRec ) == TRUE )
				{
					// Set the Timeout Period
					//status = zclSetAttrTimeoutPeriod( pAttr, cfgReportCmd );
					status = ZCL_STATUS_UNSUPPORTED_ATTRIBUTE; // for now
				}
				else
				{
					// Reports of attribute cannot be received
					status = ZCL_STATUS_UNSUPPORTED_ATTRIBUTE;
				}
			}
		}
		else
		{
			// Attribute is not supported
			status = ZCL_STATUS_UNSUPPORTED_ATTRIBUTE;
		}

		// If not successful then record the status
		if ( status != ZCL_STATUS_SUCCESS )
		{
			cfgReportRspCmd->attrList[j].status = status;
			cfgReportRspCmd->attrList[j++].attrID = reportRec->attrID;
		}
	} // for loop

	if ( j == 0 )
	{
		// Since all attributes were configured successfully, include a single
		// attribute status record in the response command with the status field
		// set to SUCCESS and the attribute ID field omitted.
		cfgReportRspCmd->attrList[0].status = ZCL_STATUS_SUCCESS;
		cfgReportRspCmd->numAttr = 1;
	}
	else
	{
		cfgReportRspCmd->numAttr = j;
	}

	// Send the response back
	zcl_SendConfigReportRspCmd( pInMsg->endPoint, &(pInMsg->srcAddr),
	pInMsg->clusterId, cfgReportRspCmd, ZCL_FRAME_SERVER_CLIENT_DIR,
	TRUE, pInMsg->zclHdr.transSeqNum );
	osal_mem_free( cfgReportRspCmd );

	return TRUE ;
}

uint8 zclSetAttrReportInterval( uint16 minReportInt ){
	minReport_32 = minReportInt;
	if(minReport_32 == 1){           //Indidation of start stress cmd
		minReport_32 = FLOOD_TIMING;
		newminReportInt = minReport_32;
		flood_fs_cntr = 0; flood_active = 1;
	}

	else if(minReport_32 > 1) {
		minReport_32 = (minReport_32*1000);
		newminReportInt = minReport_32;
		app_storage.last_rpt_interval = newminReportInt;
		osal_nv_write(NV_APP_DATA, 0, sizeof(app_storage), &app_storage);
		osal_start_timerEx( zclQDimmer_TaskID, SOLARMON_REPORT_EVT,FLOOD_TIMING);  //Send first report immediately
		return SUCCESS;
	}
	else
	stop_flood();
	
	osal_start_timerEx( zclQDimmer_TaskID, SOLARMON_REPORT_EVT,minReport_32);
	return SUCCESS;

}

void stop_flood()
{
	osal_nv_read( NV_APP_DATA, 0, sizeof(app_storage), &app_storage );
	newminReportInt = app_storage.last_rpt_interval;
	minReport_32 = newminReportInt;
	flood_fs_cntr = 0; 
	flood_active = 0;
}
#endif

#if defined (HAL_RTC)
void apply_fail_safe_sched(void)
{
	if (app_storage.dim_sched[cur_idx].dim_value !=  zclQDimmerData[0][0]){
		zclQDimmerData[0][0] = app_storage.dim_sched[cur_idx].dim_value;
		UpdateDutyCycle_grad(zclQDimmerData[0][0], EMERGENCY_OFF, PWM_PIN, app_storage.last_dimm_value[0]);
		app_storage.last_dimm_value[0] = zclQDimmerData[0][0];
		osal_nv_write(NV_APP_DATA, 0, sizeof(struct app_storage_st), &app_storage);
	}
}

void check_fail_safe_idx(void)
{
	uint8 rtc_status = DS1307_GetTime(&hh,&mm,&ss);
	zclCounter_dimmer[0][RTC_STATUS_OFFSET] = hh; //RTC is bad
	zclCounter_dimmer[0][RTC_STATUS_OFFSET+1] = mm; //RTC is bad

	if(rtc_status){
//TODO: Turn on the light and go!!
		fail_safe_mode = FALSE;
		fs_cntr = 0;
		return;
	}
//TODO: Why you need to read here?
	osal_nv_read( NV_APP_DATA, 0, sizeof(app_storage), &app_storage );

	if(++fs_cntr > MAX_FS_COUNT)
		fs_cntr = 0;	//ToDo how did we get here?
	
	if((fs_cntr == MAX_FS_COUNT) && data_sent_flag && !fail_safe_mode)
		fail_safe_mode = TRUE;

	if(roll_ovr && !cur_idx){
		if(hh == 0 )
			roll_ovr=0; 		//clear roll over
		return;
	}

	if (app_storage.dim_sched[cur_idx].mm <= mm && app_storage.dim_sched[cur_idx].hh <= hh){

		if(fail_safe_mode == TRUE && roll_ovr == 0)
			apply_fail_safe_sched();

		
		if (++cur_idx == app_storage.cur_max_sched){
			cur_idx =0 ; 
			roll_ovr= 1;	  //roll over
		}

		osal_nv_write(NV_APP_DATA, 0, sizeof(struct app_storage_st), &app_storage);
	}
}
void adjust_fail_schedule_idx(void)
{
#if defined (HAL_RTC_RST)
	if(app_storage.rtc_idx)
		osal_memcpy(app_storage.dim_sched, rtc_dim_sched[app_storage.rtc_idx].dim_tim, sizeof(app_storage.dim_sched));
#endif

	uint8 result = DS1307_GetTime(&hh,&mm,&ss);
	zclCounter_dimmer[0][RTC_STATUS_OFFSET] = hh; //RTC is bad
	zclCounter_dimmer[0][RTC_STATUS_OFFSET+1] = mm; //RTC is bad
	int8 j=app_storage.cur_max_sched-1;
	cur_idx = 0;			  //Adjust the current index
	roll_ovr = 1;

	if(result || (fail_safe_mode == FALSE)){
		fs_cntr = 0;
		return;
	}

	for(; j>=0; j--){		
		if(app_storage.dim_sched[j].hh < hh)
			continue;
			
		else if(app_storage.dim_sched[j].hh == hh && app_storage.dim_sched[j].mm <= mm)
			continue;

		else
		{
			cur_idx = j;			//Adjust the current index		
			roll_ovr = 0;
		}       
	}
	if (cur_idx)
		cur_idx--;
	else {
		cur_idx = app_storage.cur_max_sched-1;
		apply_fail_safe_sched();
		cur_idx=0;
		return;
	}
	apply_fail_safe_sched();
}             
#endif

#if defined (HAL_RTC_RST) && defined ( NV_RESTORE )
#define PROTO_PULSE_WIDTH	10 		//5 seconds	
//#define MAX_POSSIBLE_WIDTH	(uint16)((3*PROTO_PULSE_WIDTH) + (3*PROTO_PULSE_WIDTH) + (MAX_SCHED_TABLES*PROTO_PULSE_WIDTH))
#define MAX_POSSIBLE_WIDTH	170
#define SEC_OFFSET			PROTO_PULSE_WIDTH/5			//This parameters 20% offsets some inaccuracies in switching

void check_inside_sm(void){

    Time rtc; Time nvram;
    rtc.hh = hh; rtc.mm = mm; rtc.ss = ss;
    nvram = app_storage.last_rst_time;
        
	convert_bcd_to_decimal (&rtc.hh, &rtc.mm, &rtc.ss);
	convert_bcd_to_decimal (&nvram.hh, &nvram.mm, &nvram.ss);
	uint16 lrst_sec = nvram.mm * 60 + nvram.ss;
	uint16 rtc_sec = rtc.mm * 60 + rtc.ss;
	uint16 diff_sec = rtc_sec - lrst_sec;
//	If we are inside state machine then:
//	1. hh have to match (do not do this over boundary line- never)
//  2. take care of roll overs (rtc mm is less than last mm). In case there was one- discard it- start all over
	if (app_storage.state != init){
		if ((rtc.hh != nvram.hh) || (nvram.mm > mm) || ((diff_sec - SEC_OFFSET) > MAX_POSSIBLE_WIDTH)){
			HalLedBlink(HAL_LED_1, 10,70,HAL_LED_DEFAULT_FLASH_TIME/2);
			app_storage.state = init;		//reset state machine
		}
	}
	HalLedBlink(HAL_LED_1,app_storage.state+2,50,HAL_LED_DEFAULT_FLASH_TIME);
	switch(app_storage.state)
		{
// Init state no verification needed. Just stay in init if last reset happened long time before		
			case init:
				app_storage.state = start;					
				break;

//It should be less than 3 start pulses but more than one pulse in width- otherwise there is a problem.
			case start:
				if (rtc_sec <= (lrst_sec + 3 * PROTO_PULSE_WIDTH) && rtc_sec > (lrst_sec + 1 * PROTO_PULSE_WIDTH))
					app_storage.state = data;
				else{
					app_storage.state = init;
					HalLedBlink(HAL_LED_1, 15,30,HAL_LED_DEFAULT_FLASH_TIME/2);
				}	
				break;

//It should be less than max schedule pulses but more than one pulse in width- otherwise there is a problem.			
			case data:				
				if (rtc_sec <= (lrst_sec + (PROTO_PULSE_WIDTH * (MAX_SCHED_TABLES+1))) && rtc_sec > (lrst_sec + 1 * PROTO_PULSE_WIDTH)){
					app_storage.state = stop;
					app_storage.rtc_tmp_idx = (diff_sec + SEC_OFFSET)/(PROTO_PULSE_WIDTH);
					if(app_storage.rtc_tmp_idx < 2){
						app_storage.state = init;
						HalLedBlink(HAL_LED_1, 20,30,HAL_LED_DEFAULT_FLASH_TIME/2);
					}
				}
				else{
					app_storage.state = init;
//					HalLedSet ( HAL_LED_1, HAL_LED_MODE_ON );
					HalLedBlink(HAL_LED_1, 25,70,HAL_LED_DEFAULT_FLASH_TIME/2);
				}
				break;

//It should be less than 3 stop pulses but more than one pulse in width- otherwise there is a problem.
			case stop:
				if (rtc_sec <= (lrst_sec + PROTO_PULSE_WIDTH * 3) && rtc_sec > (lrst_sec + 1 * PROTO_PULSE_WIDTH)){

					--app_storage.rtc_tmp_idx;

					if (app_storage.rtc_tmp_idx == MAX_SCHED_TABLES-1){
						DS1307_SetTime(0x15, 0x15, 0x15);
					}
					else {
					app_storage.rtc_idx = app_storage.rtc_tmp_idx;
					app_storage.cur_max_sched = rtc_dim_sched[app_storage.rtc_idx].no_schedules;
					adjust_fail_schedule_idx();
					}
					app_storage.state = init;
					HalLedBlink(HAL_LED_1,app_storage.rtc_idx,70,HAL_LED_DEFAULT_FLASH_TIME/2);
				}
				else{
					HalLedBlink(HAL_LED_1, 30,70,HAL_LED_DEFAULT_FLASH_TIME/2);
					app_storage.state = init;
				}	
				break;
				
			default:
				break;		//It should never come here
		}

// Save the updated state in NVRAM		
		app_storage.last_rst_time.hh = hh;
		app_storage.last_rst_time.mm = mm;
		app_storage.last_rst_time.ss = ss;
		osal_nv_write(NV_APP_DATA, 0, sizeof(app_storage), &app_storage);
		zclCounter_dimmer[0][RTC_STATUS_OFFSET+2] = app_storage.state; //Selected index
		osal_start_timerEx( zclQDimmer_TaskID, SOLARMON_REPORT_EVT, FLOOD_TIMING);
		
}

#define CONVERT_BCD_TO_DECI(tt)	(*tt & 0xf) + ((*tt >> 4) & 0xf)*10 
void convert_bcd_to_decimal (uint8 *hhc, uint8 *mmc, uint8 *ssc){
	*hhc = CONVERT_BCD_TO_DECI(hhc);
	*mmc = CONVERT_BCD_TO_DECI(mmc);
	*ssc = CONVERT_BCD_TO_DECI(ssc);
}

#endif

void clearnv_reinit()
{
	// Wipe out the network state in NV
	NLME_InitNV();
	NLME_SetDefaultNV();
	// Set the NV startup option to force a "new" join.
	zgWriteStartupOptions( ZG_STARTUP_SET, ZCD_STARTOPT_DEFAULT_NETWORK_STATE );

	// Note: there will be no return from this call
	SystemResetSoft();

}

#if defined (ZCL_EZMODE)
/*********************************************************************
 * @fn      zclQDimmer_ProcessZDOMsgs
 *
 * @brief   Called when this node receives a ZDO/ZDP response.
 *
 * @param   none
 *
 * @return  status
 */
static void zclQDimmer_ProcessZDOMsgs( zdoIncomingMsg_t *pMsg )
{
  zclEZMode_ActionData_t data;
  ZDO_MatchDescRsp_t *pMatchDescRsp;

  // Let EZ-Mode know of the Match Descriptor Response
  if ( pMsg->clusterID == Match_Desc_rsp )
  {
    pMatchDescRsp = ZDO_ParseEPListRsp( pMsg );
    data.pMatchDescRsp = pMatchDescRsp;
    zcl_EZModeAction( EZMODE_ACTION_MATCH_DESC_RSP, &data );
    osal_mem_free( pMatchDescRsp );
  }
}

/*********************************************************************
 * @fn      zclQDimmer_EZModeCB
 *
 * @brief   The Application is informed of events. This can be used to show on the UI what is
*           going on during EZ-Mode steering/finding/binding.
 *
 * @param   state - an
 *
 * @return  none
 */
static void zclQDimmer_EZModeCB( zlcEZMode_State_t state, zclEZMode_CBData_t *pData )
{
#ifdef LCD_SUPPORTED
  char *pStr;
  uint8 err;
#endif

  // time to go into identify mode
  if ( state == EZMODE_STATE_IDENTIFYING )
  {
#ifdef LCD_SUPPORTED
    HalLcdWriteString( "EZMode", HAL_LCD_LINE_2 );
#endif

    zclQDimmer_IdentifyTime = (EZMODE_TIME / 1000);  // convert to seconds
    zclQDimmer_ProcessIdentifyTimeChange();
  }

  // autoclosing, show what happened (success, cancelled, etc...)
  if( state == EZMODE_STATE_AUTOCLOSE )
  {
#ifdef LCD_SUPPORTED
    pStr = NULL;
    err = pData->sAutoClose.err;
    if ( err == EZMODE_ERR_SUCCESS )
    {
      pStr = "EZMode: Success";
    }
    else if ( err == EZMODE_ERR_NOMATCH )
    {
      pStr = "EZMode: NoMatch"; // not a match made in heaven
    }
    if ( pStr )
    {
      if ( giDoorLockScreenMode == DOORLOCK_MAINMODE )
        HalLcdWriteString ( pStr, HAL_LCD_LINE_2 );
    }
#endif
  }

  // finished, either show DstAddr/EP, or nothing (depending on success or not)
  if( state == EZMODE_STATE_FINISH )
  {
    // turn off identify mode
    zclQDimmer_IdentifyTime = 0;
    zclQDimmer_ProcessIdentifyTimeChange();

  }

}

/*********************************************************************
* @fn      zclQDimmer_ProcessInReportCmd
*
* @brief   Process the "Profile" Discover Response Command
*
* @param   pInMsg - incoming message to process
*
* @return  none
*/
uint8 zclQDimmer_ProcessInReportCmd( zclIncomingMsg_t *pInMsg )
{
	zclReportCmd_t *InReportCmd; 

	InReportCmd = pInMsg->attrCmd;

	if (pInMsg->clusterId == ZCL_CLUSTER_ID_WISYS_CLUSTER && \
		InReportCmd->attrList[1].attrID == ATTRID_WISYS_MS_PDS_MEASURED_VALUE)
	{
		pds_triggered[0] = InReportCmd->attrList[1].attrData[0];
		osal_start_timerEx( zclQDimmer_TaskID, PDS_REPORT_EVT, PDS_OCCUPANCY_INTERVAL);
	}
	return TRUE;
}
void initiate_binding (void)
{
#if defined (HAL_RTC_RST)
	app_storage.rtc_idx = 1;
	return;
#endif	
		// Invoke EZ-Mode
	zclEZMode_InvokeData_t ezModeData;
		// Invoke EZ-Mode
	ezModeData.endpoint = DIMMER_ENDPOINT1; // endpoint on which to invoke EZ-Mode
	if ( ( zclQDimmer_NwkState == DEV_ZB_COORD ) ||
		 ( zclQDimmer_NwkState == DEV_ROUTER )	 ||
		 ( zclQDimmer_NwkState == DEV_END_DEVICE ) )
	{
	  ezModeData.onNetwork = TRUE;		// node is already on the network
	}
	else
	{
	  ezModeData.onNetwork = FALSE; 	// node is not yet on the network
	}
	ezModeData.initiator = FALSE;		 // DoorLock Device is a target
	zcl_InvokeEZMode( &ezModeData );	
}
#else
void initiate_binding (void)
{
}
#endif // ZCL_EZMODE

/****************************************************************************
****************************************************************************/


