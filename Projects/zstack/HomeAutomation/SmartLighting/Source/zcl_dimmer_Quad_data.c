/**************************************************************************************************
Filename:       zcl_dimmer_data.c
Revised:        $Date: 2008-03-11 11:01:35 -0700 (Tue, 11 Mar 2008) $
Revision:       $Revision: 16570 $


Description:    Zigbee Cluster Library - sample device application.


Copyright 2006-2007 Texas Instruments Incorporated. All rights reserved.

IMPORTANT: Your use of this Software is limited to those specific rights
granted under the terms of a software license agreement between the user
who downloaded the software, his/her employer (which must be your employer)
and Texas Instruments Incorporated (the "License").  You may not use this
Software unless you agree to abide by the terms of the License. The License
limits your use, and you acknowledge, that the Software may not be modified,
copied or distributed unless embedded on a Texas Instruments microcontroller
or used solely and exclusively in conjunction with a Texas Instruments radio
frequency transceiver, which is integrated into your product.  Other than for
the foregoing purpose, you may not use, reproduce, copy, prepare derivative
works of, modify, distribute, perform, display or sell this Software and/or
its documentation for any purpose.

YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, 
INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE, 
NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

Should you have any questions regarding your right to use this Software,
contact Texas Instruments Incorporated at www.TI.com. 
**************************************************************************************************/

/*********************************************************************
* INCLUDES
*/
#include "Global_defines.h"
#include "ZComDef.h"
#include "OSAL.h"
#include "AF.h"
#include "ZDConfig.h"

#include "zcl.h"
#include "zcl_general.h"
#include "zcl_ha.h"
#include "zcl_ms.h"
#include "zcl_dimmer_quad.h"
#include "sensirion_ht.h"
#include "HTC_Sensor.h"

/*********************************************************************
* CONSTANTS
*/

#define SAMPLELIGHT_DEVICE_VERSION     0
#define SAMPLELIGHT_FLAGS              0

#define SAMPLELIGHT_HWVERSION          1
#define SAMPLELIGHT_ZCLVERSION         1

/*********************************************************************
* TYPEDEFS
*/

/*********************************************************************
* MACROS
*/

/*********************************************************************
* GLOBAL VARIABLES
*/

// Basic Cluster
const uint8 zclQDimmer_HWRevision = SAMPLELIGHT_HWVERSION;
const uint8 zclQDimmer_ZCLVersion = SAMPLELIGHT_ZCLVERSION;
const uint8 zclQDimmer_ManufacturerName[] = { 16, 'T','e','x','a','s','I','n','s','t','r','u','m','e','n','t','s' };
const uint8 zclQDimmer_ModelId[] = { 16, 'T','I','0','0','0','1',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ' };
const uint8 zclQDimmer_DateCode[] = { 16, '2','0','0','6','0','8','3','1',' ',' ',' ',' ',' ',' ',' ',' ' };
const uint8 zclQDimmer_PowerSource = POWER_SOURCE_MAINS_1_PHASE;

uint8 zclQDimmer_LocationDescription[17] = { 16, ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ' };
uint8 zclQDimmer_PhysicalEnvironment = 0;
uint8 zclQDimmer_DeviceEnable = DEVICE_ENABLED;

// Identify Cluster
uint16 zclQDimmer_IdentifyTime = 0;

// On/Off Cluster
uint8  zclQDimmer_OnOff = LIGHT_OFF;

#if defined (HAL_RTC) 
uint8  zclRTCTable[] = {16, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0};
#endif

uint8  zclCounter_dimmer[NUMBER_OF_LIGHT_PORTS][5];
uint8  zclQDimmerData[NUMBER_OF_LIGHT_PORTS][2];

#if defined (HAL_PDS)  || defined (HAL_PDS_EXT)    
uint8  zclPdsData[NUMBER_OF_LIGHT_PORTS][2];
uint16  zclOtoUDelay;	
uint8  zcldfltdemi;	
#endif

/*********************************************************************
* ATTRIBUTE DEFINITIONS - Uses REAL cluster IDs
*/
/*********************************************************************
* SIMPLE DESCRIPTOR
*/
// This is the Cluster ID List and should be filled with Application
// specific cluster IDs.
/*********************************************************************/

CONST zclAttrRec_t zclQDimmer_Attrs[] =
{
	{
		ZCL_CLUSTER_ID_WISYS_CLUSTER,
		{ // Attribute record
			ATTRID_WISYS_MGNT_VALUE,
			ZCL_DATATYPE_UINT40,
			ACCESS_CONTROL_READ,
			(void *)zclCounter_dimmer
		}
	},
#if defined (HAL_RTC) 
	{
		ZCL_CLUSTER_ID_WISYS_CLUSTER,
		{ // Attribute record
			ATTRID_WISYS_SCHED_TABLE,
			ZCL_DATATYPE_CHAR_STR,
			ACCESS_CONTROL_WRITE,
			(void *)zclRTCTable
		}
	},
#endif
	// *** DIMMER Cluster Attributes ***
	{
		ZCL_CLUSTER_ID_GEN_ANALOG_INPUT_BASIC,
		{ // Attribute record
			ATTRID_WISYS_PWM_BASIC_PRESENT_VALUE,
			ZCL_DATATYPE_UINT16,
			ACCESS_CONTROL_READ,
			(void *)zclQDimmerData
		}
	},	
#if defined (HAL_PDS) || defined (HAL_PDS_EXT)
	// *** PDS Cluster Attributes ***
	{
		ZCL_CLUSTER_ID_MS_OCCUPANCY_SENSING,
		{ // Attribute record
			ATTRID_WISYS_MS_PDS_MEASURED_VALUE,
			ZCL_DATATYPE_UINT16,
			ACCESS_CONTROL_READ,
			(void *)zclPdsData
		}
	},  
#endif

#ifdef ZCL_EZMODE
// *** Identify Cluster Attribute ***
{
  ZCL_CLUSTER_ID_GEN_IDENTIFY,
  { // Attribute record
	ATTRID_IDENTIFY_TIME,
	ZCL_DATATYPE_UINT16,
	(ACCESS_CONTROL_READ | ACCESS_CONTROL_WRITE),
	(void *)&zclQDimmer_IdentifyTime
  }
},
#endif

};

#define ZCL_MAX_QDIMMER_INCLUSTERS       3
const cId_t zcl_DIMMER_InClusterList[ZCL_MAX_QDIMMER_INCLUSTERS] =
{
	ZCL_CLUSTER_ID_WISYS_CLUSTER,
	ZCL_CLUSTER_ID_GEN_LEVEL_CONTROL,
	ZCL_CLUSTER_ID_MS_OCCUPANCY_SENSING
};


#if defined (HAL_PDS) || defined (HAL_PDS_EXT) 
#define ZCL_MAX_QDIMMER_OUTCLUSTERS       4
const cId_t zcl_DIMMER_OutClusterList[ZCL_MAX_QDIMMER_OUTCLUSTERS] =
{
	ZCL_CLUSTER_ID_WISYS_CLUSTER,
	ZCL_CLUSTER_ID_GEN_TIME,
	ZCL_CLUSTER_ID_GEN_ANALOG_INPUT_BASIC,
	ZCL_CLUSTER_ID_MS_OCCUPANCY_SENSING
};
#else
#define ZCL_MAX_QDIMMER_OUTCLUSTERS       3
const cId_t zcl_DIMMER_OutClusterList[ZCL_MAX_QDIMMER_OUTCLUSTERS] =
{
	ZCL_CLUSTER_ID_WISYS_CLUSTER,
	ZCL_CLUSTER_ID_GEN_TIME,
	ZCL_CLUSTER_ID_GEN_ANALOG_INPUT_BASIC
};


#endif

SimpleDescriptionFormat_t zcl_DimmerDesc[] = 
{
	{
		DIMMER_ENDPOINT1,                  //  int Endpoint;
		ZCL_HA_PROFILE_ID,                     //  uint16 AppProfId[2];
		ZCL_HA_DEVICEID_DIMMABLE_LIGHT,        //  uint16 AppDeviceId[2];
		SAMPLELIGHT_DEVICE_VERSION,            //  int   AppDevVer:4;
		SAMPLELIGHT_FLAGS,                     //  int   AppFlags:4;
		ZCL_MAX_QDIMMER_INCLUSTERS,         //  byte  AppNumInClusters;
		(cId_t *)zcl_DIMMER_InClusterList, //  byte *pAppInClusterList;
		ZCL_MAX_QDIMMER_OUTCLUSTERS,        //  byte  AppNumInClusters;
		(cId_t *)zcl_DIMMER_OutClusterList //  byte *pAppInClusterList;
	},
#if defined (HAL_DIMMER_QUAD)	
	{
		DIMMER_ENDPOINT2,                  //  int Endpoint;
		ZCL_HA_PROFILE_ID,                     //  uint16 AppProfId[2];
		ZCL_HA_DEVICEID_DIMMABLE_LIGHT,        //  uint16 AppDeviceId[2];
		SAMPLELIGHT_DEVICE_VERSION,            //  int   AppDevVer:4;
		SAMPLELIGHT_FLAGS,                     //  int   AppFlags:4;
		ZCL_MAX_QDIMMER_INCLUSTERS,         //  byte  AppNumInClusters;
		(cId_t *)zcl_DIMMER_InClusterList, //  byte *pAppInClusterList;
		ZCL_MAX_QDIMMER_OUTCLUSTERS,        //  byte  AppNumInClusters;
		(cId_t *)zcl_DIMMER_OutClusterList //  byte *pAppInClusterList;
	},
	{
		DIMMER_ENDPOINT3,                  //  int Endpoint;
		ZCL_HA_PROFILE_ID,                     //  uint16 AppProfId[2];
		ZCL_HA_DEVICEID_DIMMABLE_LIGHT,        //  uint16 AppDeviceId[2];
		SAMPLELIGHT_DEVICE_VERSION,            //  int   AppDevVer:4;
		SAMPLELIGHT_FLAGS,                     //  int   AppFlags:4;
		ZCL_MAX_QDIMMER_INCLUSTERS,         //  byte  AppNumInClusters;
		(cId_t *)zcl_DIMMER_InClusterList, //  byte *pAppInClusterList;
		ZCL_MAX_QDIMMER_OUTCLUSTERS,        //  byte  AppNumInClusters;
		(cId_t *)zcl_DIMMER_OutClusterList //  byte *pAppInClusterList;
	},
	{
		DIMMER_ENDPOINT4,                  //  int Endpoint;
		ZCL_HA_PROFILE_ID,                     //  uint16 AppProfId[2];
		ZCL_HA_DEVICEID_DIMMABLE_LIGHT,        //  uint16 AppDeviceId[2];
		SAMPLELIGHT_DEVICE_VERSION,            //  int   AppDevVer:4;
		SAMPLELIGHT_FLAGS,                     //  int   AppFlags:4;
		ZCL_MAX_QDIMMER_INCLUSTERS,         //  byte  AppNumInClusters;
		(cId_t *)zcl_DIMMER_InClusterList, //  byte *pAppInClusterList;
		ZCL_MAX_QDIMMER_OUTCLUSTERS,        //  byte  AppNumInClusters;
		(cId_t *)zcl_DIMMER_OutClusterList //  byte *pAppInClusterList;
	}
#endif		
};



/*********************************************************************
* GLOBAL FUNCTIONS
*/

/*********************************************************************
* LOCAL FUNCTIONS
*/

/****************************************************************************
****************************************************************************/


