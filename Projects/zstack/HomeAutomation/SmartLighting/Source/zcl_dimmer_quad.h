/**************************************************************************************************
Filename:       zcl_dimmer.h
Revised:        $Date: 2009-12-29 18:31:22 -0800 (Tue, 29 Dec 2009) $
Revision:       $Revision: 21416 $

Description:    This file contains the Zigbee Cluster Library Home
				Automation Sample Application.


Copyright 2006-2007 Texas Instruments Incorporated. All rights reserved.

IMPORTANT: Your use of this Software is limited to those specific rights
granted under the terms of a software license agreement between the user
who downloaded the software, his/her employer (which must be your employer)
and Texas Instruments Incorporated (the "License").  You may not use this
Software unless you agree to abide by the terms of the License. The License
limits your use, and you acknowledge, that the Software may not be modified,
copied or distributed unless embedded on a Texas Instruments microcontroller
or used solely and exclusively in conjunction with a Texas Instruments radio
frequency transceiver, which is integrated into your product.  Other than for
the foregoing purpose, you may not use, reproduce, copy, prepare derivative
works of, modify, distribute, perform, display or sell this Software and/or
its documentation for any purpose.

YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, 
INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE, 
NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

Should you have any questions regarding your right to use this Software,
contact Texas Instruments Incorporated at www.TI.com. 
**************************************************************************************************/

#ifndef ZCL_SAMPLELIGHT_H
#define ZCL_SAMPLELIGHT_H

#ifdef __cplusplus
extern "C"
{
#endif

	/*********************************************************************
* INCLUDES
*/
#include "Global_defines.h"
#include "zcl.h"
	/*********************************************************************
* CONSTANTS
*/
#if (defined (HAL_DIMMER) || defined (HAL_DIMMER_QUAD)) && (!defined (HAL_DIMMER_PYROTECH) && !defined (HAL_DIMMER_SYNCO) && !defined (HAL_DIMMER_AVNI))
    #error Please define vendor
#endif

#if defined (HAL_PDS_EXT) && !defined (ZCL_EZMODE)
    #error Please define ZCL_EZMODE and ZCL_IDENTIFY
#endif

#if defined (HAL_DIMMER_PYROTECH)
	#define PWM_PIN	0
#elif defined (HAL_DIMMER_AVNI)
	#define PWM_PIN 1
#endif

#if defined (HAL_DIMMER) || defined (HAL_DIMMER_QUAD) 
extern SimpleDescriptionFormat_t zcl_DimmerDesc[];
extern CONST zclAttrRec_t zclQDimmer_Attrs[];
#define DIMMER_ENDPOINT1			8
#define DEFAULT_DIMMING_UNOCCUPIED1      							0
#endif

#define DIMMER_MAX_ATTRIBUTESX      	2

#if defined (HAL_DIMMER)
#define QUAD_ENABLE     											0
#define NUMBER_OF_LIGHT_PORTS										1
#endif  

#if defined (HAL_DIMMER_QUAD)  
#define QUAD_ENABLE     											1
#define NUMBER_OF_LIGHT_PORTS										4
#define DIMMER_ENDPOINT2            9
#define DIMMER_ENDPOINT3            10
#define DIMMER_ENDPOINT4            11

#define DEFAULT_DIMMING_UNOCCUPIED2      							25
#define DEFAULT_DIMMING_UNOCCUPIED3      							50
#define DEFAULT_DIMMING_UNOCCUPIED4      							50
#endif  

#if defined (HAL_PDS) || defined (HAL_PDS_EXT)
#define PDS_ATTRIBUTES      	1
#else
#define PDS_ATTRIBUTES      	0
#endif  

#if defined (HAL_RTC) 
#define RTC_ATTRIBUTES      	1
#else
#define RTC_ATTRIBUTES      	0
#endif  

#ifdef ZCL_EZMODE
#define EZ_ATTRIBUTES      	1
#else
#define EZ_ATTRIBUTES      	0
#endif  

#define DIMMER_MAX_ATTRIBUTES      	DIMMER_MAX_ATTRIBUTESX+PDS_ATTRIBUTES+RTC_ATTRIBUTES+EZ_ATTRIBUTES

#if defined (HAL_DIMMER_ADC)
#define DIMMER_NO_TRY	5
#endif

#define LIGHT_OFF                       0x00
#define LIGHT_ON                        0x01
#define NV_APP_DATA                     0x0401

	// Events for the sample app
#define SAMPLELIGHT_IDENTIFY_TIMEOUT_EVT         0x0001
#define SOLARMON_REPORT_EVT         0x0002
#define SOLAR_MIN_REPORTING_INTERVAL 1
#define PDS_REPORT_EVT         0x0004

#define PDS_REPORT_EVT0         0x0004
#define PDS_REPORT_EVT1         0x0008
#define PDS_REPORT_EVT2         0x0010
#define PDS_REPORT_EVT3         0x0020

	// Application Events
#define QDIMMER_IDENTIFY_TIMEOUT_EVT         0x0040
#define QDIMMER_EZMODE_NEXTSTATE_EVT         0x0080
#define QDIMMER_EZMODE_TIMEOUT_EVT           0x0100

/*********************************************************************
* MACROS
*/
	/*********************************************************************
* TYPEDEFS
*/

	/*********************************************************************
* VARIABLES
*/

	extern uint8  zclQDimmer_OnOff;

	extern uint16 zclQDimmer_IdentifyTime;

	/*********************************************************************
* FUNCTIONS
*/

	/*
* Initialization for the task
*/
	extern void zclQDimmer_Init( byte task_id );

	/*
*  Event Process for the task
*/
	extern UINT16 zclQDimmer_event_loop( byte task_id, UINT16 events );


	/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* ZCL_SAMPLELIGHT_H */
