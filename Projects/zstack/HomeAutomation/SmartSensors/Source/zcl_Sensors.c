/**************************************************************************************************
Filename:       zcl_sensors.c
Revised:        $Date: 2009-03-18 15:56:27 -0700 (Wed, 18 Mar 2009) $
Revision:       $Revision: 19453 $

Description:    Zigbee Cluster Library - sensors application.

Copyright 2006-2009 Texas Instruments Incorporated. All rights reserved.

IMPORTANT: Your use of this Software is limited to those specific rights
granted under the terms of a software license agreement between the user
who downloaded the software, his/her employer (which must be your employer)
and Texas Instruments Incorporated (the "License").  You may not use this
Software unless you agree to abide by the terms of the License. The License
limits your use, and you acknowledge, that the Software may not be modified,
copied or distributed unless embedded on a Texas Instruments microcontroller
or used solely and exclusively in conjunction with a Texas Instruments radio
frequency transceiver, which is integrated into your product.  Other than for
the foregoing purpose, you may not use, reproduce, copy, prepare derivative
works of, modify, distribute, perform, display or sell this Software and/or
its documentation for any purpose.

YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

Should you have any questions regarding your right to use this Software,
contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/*********************************************************************
This device will be like a Light device.  This application is not
intended to be a Light device, but will use the device description
to implement this sample code.
*********************************************************************/

/*********************************************************************
* INCLUDES
*/
#include "ZComDef.h"
#include "OSAL.h"
#include "AF.h"
#include "ZDApp.h"
#include "Rtg.h"
#include "zcl.h"
#include "zcl_general.h"
#include "zcl_ha.h"
#include "zcl_ezmode.h"
#include "zcl_ms.h"
#include "ZDObject.h"
#include "ZDProfile.h"
#include "OSAL_NV.h"
#include "onboard.h"
#include "hal_led.h"
#include "hal_key.h"
#include "hal_adc.h"

#include "zcl_Sensors.h"
#include "pds.h"
#include "water_meter.h"
#include "sensirion_ht.h"
#include "HTC_Sensor.h"
#include "wisys_timer.h"
#include "rtc.h"

/*********************************************************************
* MACROS
*/
#define RTC_TEST        0

/*********************************************************************
* CONSTANTS
*/
/*********************************************************************
* TYPEDEFS
*/

/*********************************************************************
* GLOBAL VARIABLES
*/
byte zclSensor_TaskID;
// There is no attribute in the Mandatory Reportable Attribute list for now
#define zcl_MandatoryReportableAttribute( a ) ( a != NULL )

static uint32 newminReportInt = 0;
static uint32 minReport_32 = 0;
static uint16 parent_addr=0;
static uint8 flood_fs_cntr = 0;
static bool flood_active = 0;
static bool zclSensor_OnOff = FALSE;
#define ZCLSAMPLELIGHT_BINDINGLIST       3
#define FLOOD_TIMING  1000
#define MAX_FLOOD_COUNT 31

/*********************************************************************
* GLOBAL FUNCTIONS
*/

/*********************************************************************
* LOCAL VARIABLES
*/
static afAddrType_t zclSensor_DstAddr;

#ifdef ZCL_EZMODE
static afAddrType_t zclSensor_BindAddr;
#endif

struct dimmer_sched{
	unsigned char 	hh;
	unsigned char	mm;
	uint8 		dim_value;
};

struct app_storage_st{
#if defined (HAL_PDS)
	uint8	last_pds_value[4];
	uint32	last_rpt_interval;
#endif
#if defined (HAL_WM)
	uint8	last_wm_reading;
#endif
};
static struct app_storage_st app_storage;

#if defined (HAL_PDS)
extern uint8 zclpdsbatdata[];
extern uint8 zclPdsData[];
extern uint8 zclCounter_pds[];
static zclReportCmd_t *pPdsReportCmd;                               // report command structure for SE Cluster
static uint8 numPdsAttr = PDS_MAX_ATTRIBUTES-1;                                        // number of SE Cluster attributes in report
static bool pds_triggered = UNOCCUPIED;
static bool pds_last_state = UNOCCUPIED;
#endif

#if defined (HAL_WM)
extern uint8 zclWmData[];
extern uint8 zclCounter_wm[];
static zclReportCmd_t *pWmReportCmd;                               // report command structure for SE Cluster
static uint8 numWmAttr = WM_MAX_ATTRIBUTES-1;                                        // number of SE Cluster attributes in report
#endif

#if defined (HAL_VALVE)
extern uint8 zclCounter_valve[];
static zclReportCmd_t *pValveReportCmd;                               // report command structure for SE Cluster
static uint8 numValveAttr = VALVE_MAX_ATTRIBUTES-1; //Not reporting time attr for now

#endif

void *App_ZdoJoinCnfCB ( void *pStr );

/*********************************************************************
* LOCAL FUNCTIONS
*/
#if defined (HAL_PDS)
static void zclSensor_HandlePDS( uint8 status );
#endif

#if defined (HAL_WM)
static void zclSensor_HandleWM(void);
#endif

void initUart(halUARTCBack_t pf);
static void zclSensor_HandleKeys( byte shift, byte keys );
static void zclSensor_BasicResetCB( void );
static void zclSensor_IdentifyCB( zclIdentify_t *pCmd );
static void zclSensor_IdentifyQueryRspCB( zclIdentifyQueryRsp_t *pRsp );
static void zclSensor_OnOffCB( uint8 cmd );
static void zclSensor_ProcessIdentifyTimeChange( void );

// Functions to process ZCL Foundation incoming Command/Response messages
static void zclSensor_ProcessIncomingMsg( zclIncomingMsg_t *msg );
static uint8 zclSensor_ProcessInConfigReportCmd( zclIncomingMsg_t *pInMsg );
static uint8 zclSetAttrReportInterval( uint16 minReportInt );
#ifdef ZCL_READ
static uint8 zclSensor_ProcessInReadRspCmd( zclIncomingMsg_t *pInMsg );
#endif
#ifdef ZCL_WRITE
static uint8 zclSensor_ProcessInWriteRspCmd( zclIncomingMsg_t *pInMsg );
static uint8 zclSensor_ProcessInWriteCmd( zclIncomingMsg_t *pInMsg );
#endif
static uint8 zclSensor_ProcessInDefaultRspCmd( zclIncomingMsg_t *pInMsg );
#ifdef ZCL_DISCOVER
static uint8 zclSensor_ProcessInDiscRspCmd( zclIncomingMsg_t *pInMsg );
#endif

void prepare_send_data(void);
void stop_flood(void);
void clearnv_reinit(void);
devStates_t zclSensor_NwkState = DEV_INIT;

#ifdef ZCL_EZMODE
uint8 zclSensorSeqNum;
static void zclSensor_ProcessZDOMsgs( zdoIncomingMsg_t *pMsg );
static void zclSensor_EZModeCB( zlcEZMode_State_t state, zclEZMode_CBData_t *pData );
static void initiate_binding(void);

static const zclEZMode_RegisterData_t zclSensor_RegisterEZModeData =
{	&zclSensor_TaskID,
	SENSOR_EZMODE_NEXTSTATE_EVT,
	SENSOR_EZMODE_TIMEOUT_EVT,
	&zclSensorSeqNum,
	zclSensor_EZModeCB
};

uint8 zclSensorSeqNum;
#endif

/*********************************************************************
* ZCL General Profile Callback table
*/
static void zclSensor_LvlControlCB (zclLCMoveToLevel_t *pCmd);
static zclGeneral_AppCallbacks_t zclSensor_CmdCallbacks =
{	zclSensor_BasicResetCB,              // Basic Cluster Reset command
	zclSensor_IdentifyCB,                // Identify command
#ifdef ZCL_EZMODE
	NULL, 								  // Identify EZ-Mode Invoke command
	NULL, 								  // Identify Update Commission State command
#endif
	NULL,
	zclSensor_IdentifyQueryRspCB,        // Identify Query Response command
	zclSensor_OnOffCB,                   // On/Off cluster command
	NULL,
	NULL,
	NULL,
	zclSensor_LvlControlCB,              // Level Control Move to Level command
	NULL,                                     // Level Control Move command
	NULL,                                     // Level Control Step command
	NULL,                                     // Level Control Stop command
#ifdef ZCL_GROUPS
	NULL,                                     // Group Response commands
#endif
#ifdef ZCL_SCENES
	NULL,                                     // Scene Store Request command
	NULL,                                     // Scene Recall Request command
	NULL,                                     // Scene Response command
#endif
#ifdef ZCL_ALARMS
	NULL,                                     // Alarm (Response) command
#endif
#ifdef SE_UK_EXT
	NULL,                                  // Get Event Log command
	NULL,                                  // Publish Event Log command
#endif
	NULL,                                  // RSSI Location command
	NULL                                   // RSSI Location Response command
};

/*********************************************************************
* @fn          zclSensor_Init
** @brief       Initialization function for the zclGeneral layer.
** @param       none
** @return      none
*/
void zclSensor_Init( byte task_id )
{	zclSensor_TaskID = task_id;
	app_storage.last_rpt_interval = DEFAULT_REPORT_INTERVAL;

	// Set destination address to indirect
	//  zclSolarMon_DstAddr.addrMode = (afAddrMode_t)AddrNotPresent;
	zclSensor_DstAddr.addrMode = (afAddrMode_t)Addr16Bit;
	zclSensor_DstAddr.endPoint = 1;
	zclSensor_DstAddr.addr.shortAddr = 0;

#ifdef ZCL_EZMODE
	zclSensor_BindAddr.addrMode = (afAddrMode_t)AddrNotPresent;
	zclSensor_BindAddr.endPoint = 0;
	zclSensor_BindAddr.addr.shortAddr = 0;
#endif
	// Register the application's attribute list
	// This app is part of the Home Automation Profile
#if defined (HAL_VALVE)
	zclHA_Init( &zcl_ValveDesc );
	zcl_registerAttrList( VALVE_ENDPOINT, VALVE_MAX_ATTRIBUTES, zclValve_Attrs );
	// Register the ZCL General Cluster Library callback functions
	zclGeneral_RegisterCmdCallbacks( VALVE_ENDPOINT, &zclSensor_CmdCallbacks );
	pValveReportCmd = (zclReportCmd_t *)osal_mem_alloc( sizeof( zclReportCmd_t ) + ( numValveAttr * sizeof( zclReport_t ) ) );
	if ( pValveReportCmd != NULL )
	{
		pValveReportCmd->numAttr = numValveAttr;
		pValveReportCmd->attrList[0].attrID = ATTRID_WISYS_MGNT_VALUE;
		pValveReportCmd->attrList[0].dataType = ZCL_DATATYPE_UINT40;
		pValveReportCmd->attrList[0].attrData = &zclCounter_valve[0];

		pValveReportCmd->attrList[1].attrID = ATTRID_WISYS_PWM_BASIC_PRESENT_VALUE;
		pValveReportCmd->attrList[1].dataType = ZCL_DATATYPE_UINT16;
		pValveReportCmd->attrList[1].attrData = &zclValveData[0];
		
	}
#endif

#if defined (HAL_PDS)
	zclHA_Init( &zcl_PdsDesc );
	zcl_registerAttrList( PDS_ENDPOINT, PDS_MAX_ATTRIBUTES, zclPds_Attrs );
	// Register the ZCL General Cluster Library callback functions
	zclGeneral_RegisterCmdCallbacks( PDS_ENDPOINT, &zclSensor_CmdCallbacks );

	pPdsReportCmd = (zclReportCmd_t *)osal_mem_alloc( sizeof( zclReportCmd_t ) + ( numPdsAttr * sizeof( zclReport_t ) ) );
	if ( pPdsReportCmd != NULL )
	{
		pPdsReportCmd->numAttr = numPdsAttr;

		pPdsReportCmd->attrList[0].attrID = ATTRID_WISYS_MGNT_VALUE;
		pPdsReportCmd->attrList[0].dataType = ZCL_DATATYPE_UINT40;
		pPdsReportCmd->attrList[0].attrData = &zclCounter_pds[0];

		pPdsReportCmd->attrList[1].attrID = ATTRID_WISYS_MS_PDS_MEASURED_VALUE;
		pPdsReportCmd->attrList[1].dataType = ZCL_DATATYPE_UINT16;
		pPdsReportCmd->attrList[1].attrData = &zclPdsData[0];

		pPdsReportCmd->attrList[2].attrID = ATTRID_WISYS_POWER_CFG_BATTERY_PERCENTAGE_REMAINING;
		pPdsReportCmd->attrList[2].dataType = ZCL_DATATYPE_UINT16;
		pPdsReportCmd->attrList[2].attrData = &zclpdsbatdata[0];
	}
#endif

#if defined (HAL_WM)
	zclHA_Init( &zcl_WmDesc );
	zcl_registerAttrList( WM_ENDPOINT, WM_MAX_ATTRIBUTES, zclWm_Attrs );
	// Register the ZCL General Cluster Library callback functions
	zclGeneral_RegisterCmdCallbacks( WM_ENDPOINT, &zclSensor_CmdCallbacks );

	pWmReportCmd = (zclReportCmd_t *)osal_mem_alloc( sizeof( zclReportCmd_t ) + ( numWmAttr * sizeof( zclReport_t ) ) );
	if ( pWmReportCmd != NULL )
	{
		pWmReportCmd->numAttr = numWmAttr;

		pWmReportCmd->attrList[0].attrID = ATTRID_WISYS_MGNT_VALUE;
		pWmReportCmd->attrList[0].dataType = ZCL_DATATYPE_UINT40;
		pWmReportCmd->attrList[0].attrData = &zclCounter_wm[0];

		pWmReportCmd->attrList[1].attrID = ATTRID_WISYS_MS_WM_MEASURED_VALUE;
		pWmReportCmd->attrList[1].dataType = ZCL_DATATYPE_UINT16;
		pWmReportCmd->attrList[1].attrData = &zclWmData[0];

	}
#endif

#if defined (HAL_DATA_LOGGER_CHINA) || defined (HAL_DATA_LOGGER_8051)
	zclHA_Init( &zcl_HtDesc );
	zcl_registerAttrList( HT_ENDPOINT, HT_MAX_ATTRIBUTES, zclHt_Attrs );
	// Register the ZCL General Cluster Library callback functions
	zclGeneral_RegisterCmdCallbacks( HT_ENDPOINT, &zclSensor_CmdCallbacks );

	pHtReportCmd = (zclReportCmd_t *)osal_mem_alloc( sizeof( zclReportCmd_t ) + ( numHtAttr * sizeof( zclReport_t ) ) );
	if ( pHtReportCmd != NULL )
	{
		pHtReportCmd->numAttr = numHtAttr;

		pHtReportCmd->attrList[0].attrID = ATTRID_WISYS_MGNT_VALUE;
		pHtReportCmd->attrList[0].dataType = ZCL_DATATYPE_UINT40;
		pHtReportCmd->attrList[0].attrData = &zclCounter_data_logger[0];
		
		pHtReportCmd->attrList[1].attrID = ATTRID_WISYS_MS_RELATIVE_HUMIDITY_MEASURED_VALUE;
		pHtReportCmd->attrList[1].dataType = ZCL_DATATYPE_UINT16;
		pHtReportCmd->attrList[1].attrData = &zclHumidData[0];

		pHtReportCmd->attrList[2].attrID = ATTRID_WISYS_MS_TEMPERATURE_MEASURED_VALUE;
		pHtReportCmd->attrList[2].dataType = ZCL_DATATYPE_UINT16;
		pHtReportCmd->attrList[2].attrData = &zclTempData[0];

		pHtReportCmd->attrList[3].attrID = ATTRID_WISYS_POWER_CFG_BATTERY_PERCENTAGE_REMAINING;
		pHtReportCmd->attrList[3].dataType = ZCL_DATATYPE_UINT16;
		pHtReportCmd->attrList[3].attrData = &zclhtbatdata[0];

		
	}
#endif

	ZMacSetTransmitPower(TX_PWR_PLUS_19);

	// Register the Application to receive the unprocessed Foundation command/response messages
	zcl_registerForMsg( zclSensor_TaskID );

	// Register for all key events - This app will handle all key events
	RegisterForKeys( zclSensor_TaskID );

	/* Register for App to receive Join Confirm */
	ZDO_RegisterForZdoCB( ZDO_JOIN_CNF_CBID, &App_ZdoJoinCnfCB );

	// Update the Boot Counter
	if ( osal_nv_item_init( NV_APP_DATA, sizeof(struct app_storage_st), &app_storage ) == ZSUCCESS )
	{
		// Get the old value from NV memory
		osal_nv_read( NV_APP_DATA, 0, sizeof(app_storage), &app_storage );
	}
	#if defined (HAL_VALVE)
	//	InitValve();          // Start PWM
#endif

#if defined (HAL_PDS)
	RegisterForpds( zclSensor_TaskID );
	pds_init(zclSensor_TaskID, 0);
#endif

#if defined (HAL_WM)
	RegisterForwm( zclSensor_TaskID );
	wm_init(zclSensor_TaskID);
#endif

#if defined (HAL_DATA_LOGGER_8051)
	ht_init();
#endif

	osal_start_timerEx( zclSensor_TaskID, SOLARMON_REPORT_EVT, 5000);
	newminReportInt =  app_storage.last_rpt_interval;

#if defined (HAL_RTC)
	DS1307_Init();

#if defined RTC_TEST
	//	TODO: Temporary stuff. Remove it later.
	for (uint8 i=0; i<4; i++){		
		app_storage.dim_sched[i].hh = 0;
		app_storage.dim_sched[i].mm = i;
		app_storage.dim_sched[i].dim_value = i*10;
	}
	osal_nv_write(NV_APP_DATA, 0, sizeof(app_storage), &app_storage);
	DS1307_SetTime(0, 0, 0);
#endif
#endif

#ifdef ZCL_EZMODE
	// Register EZ-Mode
	zcl_RegisterEZMode( &zclSensor_RegisterEZModeData );

	// Register with the ZDO to receive Match Descriptor Responses
	ZDO_RegisterForZDOMsg(task_id, Match_Desc_rsp);
#endif
}

/***************************************************************************************************
* @fn          MT_ZdoJoinCnfCB
** @brief       Handle the ZDO Join Confirm from ZDO
** @param       pStr - pointer to a parameter and a structure of parameters
** @return      void
***************************************************************************************************/
void *App_ZdoJoinCnfCB ( void *pStr )
{	/* pStr: zdoJoinCnf_t* */
	/* Packet Format */
	/* Status (1) | device addr (2) | parent addr (2) */

	/* Join Complete. De-register the callback with ZDO */
	// ZDO_DeregisterForZdoCB( ZDO_JOIN_CNF_CBID );

	return NULL;
}

/*********************************************************************
* @fn          zclSample_event_loop
** @brief       Event Loop Processor for zclGeneral.
** @param       none
** @return      none
*/
uint16 zclSensor_event_loop( uint8 task_id, uint16 events )
{	afIncomingMSGPacket_t *MSGpkt; //static int counter=0;
	(void)task_id;  // Intentionally unreferenced parameter

	if ( events & SYS_EVENT_MSG )
	{
		while ( (MSGpkt = (afIncomingMSGPacket_t *)osal_msg_receive( zclSensor_TaskID )) )
		{
			switch ( MSGpkt->hdr.event )
			{
			case ZCL_INCOMING_MSG:
				// Incoming ZCL Foundation command/response messages
				zclSensor_ProcessIncomingMsg( (zclIncomingMsg_t *)MSGpkt );
				break;

			case ZDO_CB_MSG:
#ifdef ZCL_EZMODE
				zclSensor_ProcessZDOMsgs( (zdoIncomingMsg_t *)MSGpkt );
#endif
				break;

			case KEY_CHANGE:
				zclSensor_HandleKeys( ((keyChange_t *)MSGpkt)->state, ((keyChange_t *)MSGpkt)->keys );
				break;

#if defined (HAL_PDS)
			case HAL_PDS_EVENT:
				zclSensor_HandlePDS(((keyChange_t *)MSGpkt)->state);
				break;
#endif

#if defined (HAL_WM)
			case WM_CHANGE:
				zclSensor_HandleWM();
				break;
#endif

			case ZDO_NWK_JOIN_REQ:
				break;
				
			case ZDO_STATE_CHANGE:
				zclSensor_NwkState = (devStates_t)(MSGpkt->hdr.status);
				break;

			default:
				break;
			}

			// Release the memory
			osal_msg_deallocate( (uint8 *)MSGpkt );
		}

		// return unprocessed events
		return (events ^ SYS_EVENT_MSG);
	}

	if ( events & SAMPLELIGHT_IDENTIFY_TIMEOUT_EVT )
	{
		if ( zclSensor_IdentifyTime > 0 )
		zclSensor_IdentifyTime--;
		zclSensor_ProcessIdentifyTimeChange();

		return ( events ^ SAMPLELIGHT_IDENTIFY_TIMEOUT_EVT );
	}

	if ( events & SOLARMON_REPORT_EVT )
	{
		prepare_send_data();
		osal_start_timerEx( zclSensor_TaskID, SOLARMON_REPORT_EVT, newminReportInt);
		return ( events ^ SOLARMON_REPORT_EVT );
	}

#if defined (HAL_PDS)
	if ( events & PDS_REPORT_EVT ){

		if (pds_triggered != pds_last_state)
		{
			pds_last_state = pds_triggered;
			app_storage.last_pds_value[0] = pds_triggered;
			prepare_send_data();
#ifdef ZCL_EZMODE
			zclPdsData[0] = pds_triggered;
			zcl_SendReportCmd( PDS_ENDPOINT, &zclSensor_BindAddr,
			ZCL_CLUSTER_ID_WISYS_CLUSTER, pPdsReportCmd,
			ZCL_FRAME_SERVER_CLIENT_DIR, 1, 0 );
#endif			
		}
		pds_triggered = UNOCCUPIED;
		osal_start_timerEx( zclSensor_TaskID, PDS_REPORT_EVT, PDS_FIRST_INTERVAL);
		return ( events ^ PDS_REPORT_EVT );
	}
#endif

#ifdef ZCL_EZMODE
	// going on to next state
	if ( events & SENSOR_EZMODE_NEXTSTATE_EVT )
	{
		zcl_EZModeAction ( EZMODE_ACTION_PROCESS, NULL );	// going on to next state
		return ( events ^ SENSOR_EZMODE_NEXTSTATE_EVT );
	}
		// the overall EZMode timer expired, so we timed out
	if ( events & SENSOR_EZMODE_TIMEOUT_EVT )
	{
		zcl_EZModeAction ( EZMODE_ACTION_TIMED_OUT, NULL ); // EZ-Mode timed out
		return ( events ^ SENSOR_EZMODE_TIMEOUT_EVT );
	}
#endif // ZLC_EZMODE

	return 0;
}
#if defined (HAL_PDS)
static void zclSensor_HandlePDS( uint8 pds_no )
{	if(pds_triggered == OCCUPIED)
	osal_start_timerEx( zclSensor_TaskID, PDS_REPORT_EVT, PDS_OCCUPANCY_INTERVAL);
	pds_triggered = OCCUPIED;
}
#endif

#if defined (HAL_WM)
static void zclSensor_HandleWM(void)
{	zclWmData[0]++;
	prepare_send_data();

	app_storage.last_wm_reading =  zclWmData[0];
	osal_nv_write(NV_APP_DATA, 0, sizeof(app_storage), &app_storage);
}
#endif

void prepare_send_data(void)
{	associated_devices_t* pAssoc; 	
	pAssoc = AssocGetWithShort(parent_addr);
	//    In case need to know the next routing entry. This ONLY works for routers, for end nodes it causes reset
	//    parent_addr = RTG_GetNextHop (0, INVALID_NODE_ADDR, self_addr, INVALID_NODE_ADDR, 0);
	//    zclCounter[1] = (counter >> 8)&0xFF;
	//    zclCounter[0] = (counter)&0xFF;
#if defined (HAL_VALVE)
	static uint16 counter_valve=0; uint16 temp16=0;

	zclValveData[1] = (temp16 >> 8)&0xFF;
	zclValveData[0] = (temp16)&0xFF;

	counter_valve++;
	zclCounter_valve[0] = (counter_valve)&0xFF;
	zclCounter_valve[1] = (counter_valve >> 8)&0xFF;
	zclCounter_valve[2] = pAssoc->linkInfo.rxLqi;
	zclCounter_valve[3] = LO_UINT16( parent_addr );
	zclCounter_valve[4] = HI_UINT16( parent_addr );

	zcl_SendReportCmd(VALVE_ENDPOINT, &zclSensor_DstAddr,
	ZCL_CLUSTER_ID_WISYS_CLUSTER, pValveReportCmd,
	ZCL_FRAME_SERVER_CLIENT_DIR, 1, 0 );
	halSleep(50);
#endif

#if defined (HAL_PDS)
	static uint16 counter_pds=0; uint16 tempx16=0;
	tempx16 = app_storage.last_pds_value[0];
	counter_pds++;
	zclCounter_pds[0] = (counter_pds)&0xFF;
	zclCounter_pds[1] = (counter_pds >> 8)&0xFF;
	zclCounter_pds[2] = pAssoc->linkInfo.rxLqi;
	zclCounter_pds[3] = LO_UINT16( parent_addr );
	zclCounter_pds[4] = HI_UINT16( parent_addr );

	zclPdsData[1] = (tempx16 >> 8)&0xFF;
	zclPdsData[0] = (tempx16)&0xFF;

	zcl_SendReportCmd( PDS_ENDPOINT, &zclSensor_DstAddr,
	ZCL_CLUSTER_ID_WISYS_CLUSTER, pPdsReportCmd,
	ZCL_FRAME_SERVER_CLIENT_DIR, 1, 0 );

	//just for testing. remove it later
#ifdef ZCL_EZMODE
//	zcl_SendReportCmd( PDS_ENDPOINT, &zclSensor_BindAddr,
//	ZCL_CLUSTER_ID_WISYS_CLUSTER, pPdsReportCmd,
//	ZCL_FRAME_SERVER_CLIENT_DIR, 1, 0 );
#endif			

#endif

#if defined (HAL_WM)
	static uint16 counter_wm=0;
	counter_wm++;
	zclCounter_wm[0] = (counter_wm)&0xFF;
	zclCounter_wm[1] = (counter_wm >> 8)&0xFF;
	zclCounter_wm[2] = pAssoc->linkInfo.rxLqi;
	zclCounter_wm[3] = LO_UINT16( parent_addr );
	zclCounter_wm[4] = HI_UINT16( parent_addr );

	zcl_SendReportCmd( WM_ENDPOINT, &zclSensor_DstAddr,
	ZCL_CLUSTER_ID_WISYS_CLUSTER, pWmReportCmd,
	ZCL_FRAME_SERVER_CLIENT_DIR, 1, 0 );
	halSleep(50);

#endif

#if defined (HAL_DATA_LOGGER_8051) || defined (HAL_DATA_LOGGER_CHINA)
	static uint16 counter_data_logger=0;
	static uint16 temp16=0;
	RH();
	counter_data_logger++;
	HalAdcSetReference(HAL_ADC_REF_125V);
	temp16 = ((HalAdcRead(HAL_ADC_CHANNEL_0, HAL_ADC_RESOLUTION_12)));
	zclCounter_data_logger[0] = (counter_data_logger)&0xFF;
	zclCounter_data_logger[1] = (counter_data_logger >> 8)&0xFF;
	zclCounter_data_logger[2] = pAssoc->linkInfo.rxLqi;
	zclCounter_data_logger[3] = LO_UINT16( parent_addr );
	zclCounter_data_logger[4] = HI_UINT16( parent_addr );

	zclCounter_data_logger[1] = (counter_data_logger >> 8)&0xFF;
	zclCounter_data_logger[0] = (counter_data_logger)&0xFF;

	
	zclHumidData[1] = U8RH_data_H;
	zclHumidData[0] = U8RH_data_L;
	zclTempData[1] = U8T_data_H;
	zclTempData[0] = U8T_data_L;

	zclhtbatdata[1] = (temp16 >> 8)&0xFF;
	zclhtbatdata[0] = (temp16)&0xFF;
		zcl_SendReportCmd( HT_ENDPOINT, &zclSensor_DstAddr,
	ZCL_CLUSTER_ID_WISYS_CLUSTER, pHtReportCmd,
	ZCL_FRAME_SERVER_CLIENT_DIR, 1, 0 );
	HalLedBlink (HAL_LED_1,3,1,HAL_LED_DEFAULT_FLASH_TIME );
	halSleep(50);
#endif

	if (flood_active && (flood_fs_cntr++ == MAX_FLOOD_COUNT))
	stop_flood();

}

/*********************************************************************
* @fn      zclSolarMon_HandleKeys
** @brief   Handles all key events for this device.
** @param   shift - true if in shift/alt.
* @param   keys - bit field for key events. Valid entries:
*                 HAL_KEY_SW_4
*                 HAL_KEY_SW_3
*                 HAL_KEY_SW_2
*                 HAL_KEY_SW_1
** @return  none
*/
void zclSensor_HandleKeys( byte shift, byte keys )
{	//  zAddrType_t dstAddr;

	(void)shift;  // Intentionally unreferenced parameter
#if !defined (HAL_CUSTOM_BOARD)
	if ( keys & HAL_KEY_SW_1 )
#else
	if ( keys & HAL_KEY_EXTSW_1 )
#endif
		{
		//    HalLedBlink ( HAL_LED_1, 0, 50, HAL_LED_DEFAULT_FLASH_TIME );
	}

	if ( keys & HAL_KEY_EXTSW_2 )
	{
#ifdef ZCL_EZMODE
		zclEZMode_InvokeData_t ezModeData;
		static uint16 clusterIDs[] = { ZCL_CLUSTER_ID_WISYS_CLUSTER };	// only bind on the on/off cluster

		// Invoke EZ-Mode
		ezModeData.endpoint = PDS_ENDPOINT; // endpoint on which to invoke EZ-Mode
		if ( ( zclSensor_NwkState == DEV_ZB_COORD ) ||
				( zclSensor_NwkState == DEV_ROUTER )   ||
				( zclSensor_NwkState == DEV_END_DEVICE ) )
		{
			ezModeData.onNetwork = TRUE;		// node is already on the network
		}
		else
		{
			ezModeData.onNetwork = FALSE; 	// node is not yet on the network
		}

		ezModeData.initiator = TRUE;			// DoorLock Controller is an initiator
		ezModeData.numActiveInClusters = 0;
		ezModeData.pActiveInClusterIDs = NULL;
		ezModeData.numActiveOutClusters = 1;	// active output cluster
		ezModeData.pActiveOutClusterIDs = clusterIDs;
		zcl_InvokeEZMode( &ezModeData );
#ifdef LCD_SUPPORTED
		HalLcdWriteString( "EZMode", HAL_LCD_LINE_2 );
#endif
		
#endif
	}

	if ( keys & HAL_KEY_SW_3 )
	{
	}

	if ( keys & HAL_KEY_SW_4 )
	{
	}
}

/*********************************************************************
* @fn      zclSensor_ProcessIdentifyTimeChange
** @brief   Called to process any change to the IdentifyTime attribute.
** @param   none
** @return  none
*/
static void zclSensor_ProcessIdentifyTimeChange( void )
{	if ( zclSensor_IdentifyTime > 0 )
	{
		osal_start_timerEx( zclSensor_TaskID, SAMPLELIGHT_IDENTIFY_TIMEOUT_EVT, 1000 );
		HalLedBlink ( HAL_LED_4, 0xFF, HAL_LED_DEFAULT_DUTY_CYCLE, HAL_LED_DEFAULT_FLASH_TIME );
	}
	else
	{
		if ( zclSensor_OnOff )
		HalLedSet ( HAL_LED_4, HAL_LED_MODE_ON );
		else
		HalLedSet ( HAL_LED_4, HAL_LED_MODE_OFF );
		osal_stop_timerEx( zclSensor_TaskID, SAMPLELIGHT_IDENTIFY_TIMEOUT_EVT );
	}
}

/*********************************************************************
* @fn      zclSensor_BasicResetCB
** @brief   Callback from the ZCL General Cluster Library
*          to set all the Basic Cluster attributes to default values.
** @param   none
** @return  none
*/
static void zclSensor_BasicResetCB( void )
{	// Reset all attributes to default values
}/*********************************************************************
* @fn      zclSensor_IdentifyCB
** @brief   Callback from the ZCL General Cluster Library when
*          it received an Identity Command for this application.
** @param   srcAddr - source address and endpoint of the response message
* @param   identifyTime - the number of seconds to identify yourself
** @return  none
*/
static void zclSensor_IdentifyCB( zclIdentify_t *pCmd )
{	zclSensor_IdentifyTime = pCmd->identifyTime;
	zclSensor_ProcessIdentifyTimeChange();
}
/*********************************************************************
* @fn      zclSensor_IdentifyQueryRspCB
** @brief   Callback from the ZCL General Cluster Library when
*          it received an Identity Query Response Command for this application.
** @param   srcAddr - requestor's address
* @param   timeout - number of seconds to identify yourself (valid for query response)
** @return  none
*/
static void zclSensor_IdentifyQueryRspCB(  zclIdentifyQueryRsp_t *pRsp )
{	// Query Response (with timeout value)
	(void)pRsp;
#ifdef ZCL_EZMODE
	{
		zclEZMode_ActionData_t data;
		data.pIdentifyQueryRsp = pRsp;
		zcl_EZModeAction ( EZMODE_ACTION_IDENTIFY_QUERY_RSP, &data );
	}
#endif
}/*********************************************************************
* @fn      zclSensor_OnOffCB
** @brief   Callback from the ZCL General Cluster Library when
*          it received an On/Off Command for this application.
** @param   cmd - COMMAND_ON, COMMAND_OFF or COMMAND_TOGGLE
** @return  none
*/
static void zclSensor_OnOffCB( uint8 cmd )
{}

static void zclSensor_LvlControlCB (zclLCMoveToLevel_t *pCmd){
#if defined (HAL_VALVE)
	zclValveData[1] = (pCmd->level >> 8)&0xFF;
	zclValveData[0] = (pCmd->level)&0xFF;

	if (pCmd->level == 0)
	P0_3=0;
	else
	P0_3=1;
	osal_nv_write(NV_APP_DATA, 0, sizeof(struct app_storage_st), &app_storage);
	osal_start_timerEx( zclSensor_TaskID, SOLARMON_REPORT_EVT,FLOOD_TIMING);  //Send first report immediately
	return;	
#endif
}

/******************************************************************************
**  Functions for processing ZCL Foundation incoming Command/Response messages
******************************************************************************/

/*********************************************************************
* @fn      zclSensor_ProcessIncomingMsg
** @brief   Process ZCL Foundation incoming message
** @param   pInMsg - pointer to the received message
** @return  none
*/
static void zclSensor_ProcessIncomingMsg( zclIncomingMsg_t *pInMsg)
{	switch ( pInMsg->zclHdr.commandID )
	{
#ifdef ZCL_READ
	case ZCL_CMD_READ_RSP:
		zclSensor_ProcessInReadRspCmd( pInMsg );
		break;
#endif
#ifdef ZCL_WRITE
	case ZCL_CMD_WRITE_RSP:
		zclSensor_ProcessInWriteRspCmd( pInMsg );
		break;
		
	case ZCL_CMD_WRITE:
		zclSensor_ProcessInWriteCmd( pInMsg );
		break;
		
#endif
#ifdef ZCL_REPORT
		// See ZCL Test Applicaiton (zcl_testapp.c) for sample code on Attribute Reporting
	case ZCL_CMD_CONFIG_REPORT:
		zclSensor_ProcessInConfigReportCmd( pInMsg );
		break;
		
	case ZCL_CMD_CONFIG_REPORT_RSP:
		//zclSensor_ProcessInConfigReportRspCmd( pInMsg );
		break;
		
	case ZCL_CMD_READ_REPORT_CFG:
		//zclSensor_ProcessInReadReportCfgCmd( pInMsg );
		break;
		
	case ZCL_CMD_READ_REPORT_CFG_RSP:
		//zclSensor_ProcessInReadReportCfgRspCmd( pInMsg );
		break;
		
	case ZCL_CMD_REPORT:
		//zclSensor_ProcessInReportCmd( pInMsg );
		break;
#endif
	case ZCL_CMD_DEFAULT_RSP:
		zclSensor_ProcessInDefaultRspCmd( pInMsg );
		break;
#ifdef ZCL_DISCOVER
	case ZCL_CMD_DISCOVER_RSP:
		zclSensor_ProcessInDiscRspCmd( pInMsg );
		break;
#endif
	default:
		break;
	}

	if ( pInMsg->attrCmd )
	osal_mem_free( pInMsg->attrCmd );
}
#ifdef ZCL_READ
/*********************************************************************
* @fn      zclSensor_ProcessInReadRspCmd
** @brief   Process the "Profile" Read Response Command
** @param   pInMsg - incoming message to process
** @return  none
*/
static uint8 zclSensor_ProcessInReadRspCmd( zclIncomingMsg_t *pInMsg )
{	zclReadRspCmd_t *readRspCmd;
	uint8 i;

	readRspCmd = (zclReadRspCmd_t *)pInMsg->attrCmd;
	for (i = 0; i < readRspCmd->numAttr; i++)
	{
		// Notify the originator of the results of the original read attributes
		// attempt and, for each successfull request, the value of the requested
		// attribute
	}

	return TRUE;
}
#endif // ZCL_READ

#ifdef ZCL_WRITE
/*********************************************************************
* @fn      zclSensor_ProcessInWriteRspCmd
** @brief   Process the "Profile" Write Response Command
** @param   pInMsg - incoming message to process
** @return  none
*/
static uint8 zclSensor_ProcessInWriteRspCmd( zclIncomingMsg_t *pInMsg )
{	zclWriteRspCmd_t *writeRspCmd;
	uint8 i;

	writeRspCmd = (zclWriteRspCmd_t *)pInMsg->attrCmd;
	for (i = 0; i < writeRspCmd->numAttr; i++)
	{
		// Notify the device of the results of the its original write attributes
		// command.
	}

	return TRUE;
}

/*********************************************************************
* @fn      zclSensor_ProcessInWriteRspCmd
** @brief   Process the "Profile" Write Response Command
** @param   pInMsg - incoming message to process
** @return  none
*/
static uint8 zclSensor_ProcessInWriteCmd( zclIncomingMsg_t *pInMsg )
{	return TRUE;
}
#endif // ZCL_WRITE
/*********************************************************************
* @fn      zclSensor_ProcessInDefaultRspCmd
** @brief   Process the "Profile" Default Response Command
** @param   pInMsg - incoming message to process
** @return  none
*/
static uint8 zclSensor_ProcessInDefaultRspCmd( zclIncomingMsg_t *pInMsg )
{	// zclDefaultRspCmd_t *defaultRspCmd = (zclDefaultRspCmd_t *)pInMsg->attrCmd;
	// Device is notified of the Default Response command.
	(void)pInMsg;
	return TRUE;
}
#ifdef ZCL_DISCOVER
/*********************************************************************
* @fn      zclSensor_ProcessInDiscRspCmd
** @brief   Process the "Profile" Discover Response Command
** @param   pInMsg - incoming message to process
** @return  none
*/
static uint8 zclSensor_ProcessInDiscRspCmd( zclIncomingMsg_t *pInMsg )
{	zclDiscoverRspCmd_t *discoverRspCmd;
	uint8 i;

	discoverRspCmd = (zclDiscoverRspCmd_t *)pInMsg->attrCmd;
	for ( i = 0; i < discoverRspCmd->numAttr; i++ )
	{
		// Device is notified of the result of its attribute discovery command.
	}

	return TRUE;
}
#endif // ZCL_DISCOVER

//TEST3
#if defined ( ZCL_REPORT )
/*********************************************************************
* @fn      simplemeter_ProcessInConfigReportCmd
** @brief   Process the "Profile" Configure Reporting Command
** @param   pInMsg - incoming message to process
** @return  TRUE if attribute was found in the Attribute list,
*          FALSE if not
*/
static uint8 zclSensor_ProcessInConfigReportCmd( zclIncomingMsg_t *pInMsg )
{	zclCfgReportCmd_t *cfgReportCmd;
	zclCfgReportRec_t *reportRec;
	zclCfgReportRspCmd_t *cfgReportRspCmd;
	zclAttrRec_t attrRec;
	uint8 status;
	uint8 i, j = 0;

	cfgReportCmd = (zclCfgReportCmd_t *)pInMsg->attrCmd;

	// Allocate space for the response command
	cfgReportRspCmd = (zclCfgReportRspCmd_t *)osal_mem_alloc( sizeof ( zclCfgReportRspCmd_t ) + \
	sizeof ( zclCfgReportStatus_t) * cfgReportCmd->numAttr );
	if ( cfgReportRspCmd == NULL )
	return FALSE; // EMBEDDED RETURN

	// Process each Attribute Reporting Configuration record
	for ( i = 0; i < cfgReportCmd->numAttr; i++ )
	{
		reportRec = &(cfgReportCmd->attrList[i]);

		status = ZCL_STATUS_SUCCESS;

		if (1)              //Assume right attribute
		{
			if ( reportRec->direction == ZCL_SEND_ATTR_REPORTS )
			{
				//        if ( reportRec->dataType == attrRec.attr.dataType )
				//        {
				// This the attribute that is to be reported
				if ( zcl_MandatoryReportableAttribute( &attrRec ) == TRUE )
				{
					// Set the Min and Max Reporting Intervals and Reportable Change
					if(reportRec->minReportInt == 1)
#ifdef ZCL_EZMODE
					initiate_binding();
#else
					{}
#endif
					else if(reportRec->minReportInt == 4)
#ifdef HAL_WM
					zclWmData[0] = 0;
#else
					{}
#endif

					else if(reportRec->minReportInt)
					status = zclSetAttrReportInterval( reportRec->minReportInt );
					else
					clearnv_reinit();

					status = ZCL_STATUS_SUCCESS; // for now
				}
			}
			else
			{
				// We shall expect reports of values of this attribute
				if ( zcl_MandatoryReportableAttribute( &attrRec ) == TRUE )
				{
					// Set the Timeout Period
					//status = zclSetAttrTimeoutPeriod( pAttr, cfgReportCmd );
					status = ZCL_STATUS_UNSUPPORTED_ATTRIBUTE; // for now
				}
				else
				{
					// Reports of attribute cannot be received
					status = ZCL_STATUS_UNSUPPORTED_ATTRIBUTE;
				}
			}
		}
		else
		{
			// Attribute is not supported
			status = ZCL_STATUS_UNSUPPORTED_ATTRIBUTE;
		}

		// If not successful then record the status
		if ( status != ZCL_STATUS_SUCCESS )
		{
			cfgReportRspCmd->attrList[j].status = status;
			cfgReportRspCmd->attrList[j++].attrID = reportRec->attrID;
		}
	} // for loop

	if ( j == 0 )
	{
		// Since all attributes were configured successfully, include a single
		// attribute status record in the response command with the status field
		// set to SUCCESS and the attribute ID field omitted.
		cfgReportRspCmd->attrList[0].status = ZCL_STATUS_SUCCESS;
		cfgReportRspCmd->numAttr = 1;
	}
	else
	{
		cfgReportRspCmd->numAttr = j;
	}

	// Send the response back
	zcl_SendConfigReportRspCmd( pInMsg->endPoint, &(pInMsg->srcAddr),
	pInMsg->clusterId, cfgReportRspCmd, ZCL_FRAME_SERVER_CLIENT_DIR,
	TRUE, pInMsg->zclHdr.transSeqNum );
	osal_mem_free( cfgReportRspCmd );

	return TRUE ;
}

uint8 zclSetAttrReportInterval( uint16 minReportInt ){
	minReport_32 = minReportInt;
	if(minReport_32 == 0){           //Indidation of start stress cmd
		minReport_32 = FLOOD_TIMING;
		newminReportInt = minReport_32;
		flood_fs_cntr = 0; flood_active = 1;
	}

	else if(minReport_32 > 1) {
		minReport_32 = (minReport_32*1000);
		newminReportInt = minReport_32;
		app_storage.last_rpt_interval = newminReportInt;
		osal_nv_write(NV_APP_DATA, 0, sizeof(app_storage), &app_storage);
		osal_start_timerEx( zclSensor_TaskID, SOLARMON_REPORT_EVT,FLOOD_TIMING);  //Send first report immediately
		return SUCCESS;
	}
	else
	stop_flood();
		osal_start_timerEx( zclSensor_TaskID, SOLARMON_REPORT_EVT,minReport_32);
	return SUCCESS;
}

void stop_flood()
{	osal_nv_read( NV_APP_DATA, 0, sizeof(app_storage), &app_storage );
	newminReportInt = app_storage.last_rpt_interval;
	minReport_32 = newminReportInt;
	flood_fs_cntr = 0; flood_active = 0;
}
#endif

void clearnv_reinit(void)
{	// Wipe out the network state in NV
	NLME_InitNV();
	NLME_SetDefaultNV();
	// Set the NV startup option to force a "new" join.
	zgWriteStartupOptions( ZG_STARTUP_SET, ZCD_STARTOPT_DEFAULT_NETWORK_STATE );

	// Note: there will be no return from this call
	SystemResetSoft();

}

#ifdef ZCL_EZMODE
/*********************************************************************
* @fn      zclSensor_ProcessZDOMsgs
** @brief   Called when this node receives a ZDO/ZDP response.
** @param   none
** @return  status
*/
static void zclSensor_ProcessZDOMsgs( zdoIncomingMsg_t *pMsg )
{	zclEZMode_ActionData_t data;
	ZDO_MatchDescRsp_t *pMatchDescRsp;

	// Let EZ-Mode know of the Match Descriptor Response
	if ( pMsg->clusterID == Match_Desc_rsp )
	{
		pMatchDescRsp = ZDO_ParseEPListRsp( pMsg );
		data.pMatchDescRsp = pMatchDescRsp;
		zcl_EZModeAction( EZMODE_ACTION_MATCH_DESC_RSP, &data );
		osal_mem_free( pMatchDescRsp );
	}
}
/*********************************************************************
* @fn      zclSensor_EZModeCB
** @brief   The Application is informed of events. This can be used to show on the UI what is
*           going on during EZ-Mode steering/finding/binding.
** @param   state - an
** @return  none
*/
static void zclSensor_EZModeCB( zlcEZMode_State_t state, zclEZMode_CBData_t *pData )
{	// time to go into identify mode
	if ( state == EZMODE_STATE_IDENTIFYING )
	{
		zclSensor_IdentifyTime = (EZMODE_TIME / 1000);  // convert to seconds
		zclSensor_ProcessIdentifyTimeChange();
	}

	// finished, either show DstAddr/EP, or nothing (depending on success or not)
	if( state == EZMODE_STATE_FINISH )
	{
		// turn off identify mode
		zclSensor_IdentifyTime = 0;
		zclSensor_ProcessIdentifyTimeChange();

	}
}

void initiate_binding(void)
{	zclEZMode_InvokeData_t ezModeData;
	static uint16 clusterIDs[] = { ZCL_CLUSTER_ID_WISYS_CLUSTER };	// only bind on the on/off cluster

	// Invoke EZ-Mode
	ezModeData.endpoint = PDS_ENDPOINT; // endpoint on which to invoke EZ-Mode
	if ( ( zclSensor_NwkState == DEV_ZB_COORD ) ||
			( zclSensor_NwkState == DEV_ROUTER )	||
			( zclSensor_NwkState == DEV_END_DEVICE ) )
	{
		ezModeData.onNetwork = TRUE;		// node is already on the network
	}
	else
	{
		ezModeData.onNetwork = FALSE; 	// node is not yet on the network
	}

	ezModeData.initiator = TRUE;			// DoorLock Controller is an initiator
	ezModeData.numActiveInClusters = 0;
	ezModeData.pActiveInClusterIDs = NULL;
	ezModeData.numActiveOutClusters = 1;	// active output cluster
	ezModeData.pActiveOutClusterIDs = clusterIDs;
	zcl_InvokeEZMode( &ezModeData );
}
#endif //

/****************************************************************************
****************************************************************************/
