/**************************************************************************************************
Filename:       zcl_dimmer_data.c
Revised:        $Date: 2008-03-11 11:01:35 -0700 (Tue, 11 Mar 2008) $
Revision:       $Revision: 16570 $

Description:    Zigbee Cluster Library - sample device application.

Copyright 2006-2007 Texas Instruments Incorporated. All rights reserved.

IMPORTANT: Your use of this Software is limited to those specific rights
granted under the terms of a software license agreement between the user
who downloaded the software, his/her employer (which must be your employer)
and Texas Instruments Incorporated (the "License").  You may not use this
Software unless you agree to abide by the terms of the License. The License
limits your use, and you acknowledge, that the Software may not be modified,
copied or distributed unless embedded on a Texas Instruments microcontroller
or used solely and exclusively in conjunction with a Texas Instruments radio
frequency transceiver, which is integrated into your product.  Other than for
the foregoing purpose, you may not use, reproduce, copy, prepare derivative
works of, modify, distribute, perform, display or sell this Software and/or
its documentation for any purpose.

YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

Should you have any questions regarding your right to use this Software,
contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/*********************************************************************
* INCLUDES
*/
#include "ZComDef.h"
#include "OSAL.h"
#include "AF.h"
#include "ZDConfig.h"

#include "zcl.h"
#include "zcl_general.h"
#include "zcl_ha.h"
#include "zcl_ms.h"
#include "zcl_Sensors.h"
#include "sensirion_ht.h"
#include "HTC_Sensor.h"

/*********************************************************************
* CONSTANTS
*/

#define SAMPLELIGHT_DEVICE_VERSION     0
#define SAMPLELIGHT_FLAGS              0

#define SAMPLELIGHT_HWVERSION          1
#define SAMPLELIGHT_ZCLVERSION         1

/*********************************************************************
* TYPEDEFS
*/

/*********************************************************************
* MACROS
*/

/*********************************************************************
* GLOBAL VARIABLES
*/

// Basic Cluster
const uint8 zclSensor_HWRevision = SAMPLELIGHT_HWVERSION;
const uint8 zclSensor_ZCLVersion = SAMPLELIGHT_ZCLVERSION;
const uint8 zclSensor_ManufacturerName[] = { 16, 'T','e','x','a','s','I','n','s','t','r','u','m','e','n','t','s' };
const uint8 zclSensor_ModelId[] = { 16, 'T','I','0','0','0','1',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ' };
const uint8 zclSensor_DateCode[] = { 16, '2','0','0','6','0','8','3','1',' ',' ',' ',' ',' ',' ',' ',' ' };
const uint8 zclSensor_PowerSource = POWER_SOURCE_MAINS_1_PHASE;

uint8 zclSensor_LocationDescription[17] = { 16, ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ' };
uint8 zclSensor_PhysicalEnvironment = 0;
uint8 zclSensor_DeviceEnable = DEVICE_ENABLED;

// Identify Cluster
uint16 zclSensor_IdentifyTime = 0;

// On/Off Cluster
//uint8  zclCounter[5] = {0x0, 0x0, 0x0, 0x0, 0x0};
uint8  zclTime[4] = {0x0, 0x0, 0x0, 0x0};
uint8  zclRTCTable[] = {16, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0};
#if defined (HAL_DATA_LOGGER_CHINA) || defined (HAL_DATA_LOGGER_8051)
uint8  zclhtbatdata[2] = {0x0, 0x0};
uint8  zclCounter_data_logger[5] = {0x0, 0x0, 0x0, 0x0, 0x0};
uint8  zclHumidData[2] = {0x0, 0x0};
uint8  zclTempData[2] = {0x0, 0x0};
#endif
#if defined (HAL_VALVE)
uint8  zclCounter_valve[5] = {0x0, 0x0, 0x0, 0x0, 0x0};
uint8  zclValveData[2] = {0x0, 0x0};
#endif
#if defined (HAL_PDS)
uint8  zclpdsbatdata[2] = {0x0, 0x0};
uint8  zclCounter_pds[5] = {0x0, 0x0, 0x0, 0x0, 0x0};
uint8  zclPdsData[2] = {0x0, 0x0};
#endif
#if defined (HAL_WM)
uint8  zclCounter_wm[5] = {0x0, 0x0, 0x0, 0x0, 0x0};
uint8  zclWmData[2] = {0x0, 0x0};
#endif

/*********************************************************************
* ATTRIBUTE DEFINITIONS - Uses REAL cluster IDs
*/
/*********************************************************************
* SIMPLE DESCRIPTOR
*/
// This is the Cluster ID List and should be filled with Application
// specific cluster IDs.
/*********************************************************************/

#if defined (HAL_PDS)

CONST zclAttrRec_t zclPds_Attrs[PDS_MAX_ATTRIBUTES] =
{	{
		ZCL_CLUSTER_ID_WISYS_CLUSTER,
		{ // Attribute record
			ATTRID_WISYS_MGNT_VALUE,
			ZCL_DATATYPE_UINT40,
			ACCESS_CONTROL_READ,
			(void *)zclCounter_pds
		}
	},
	{
		ZCL_CLUSTER_ID_GEN_TIME,
		{ // Attribute record
			ATTRID_TIME_TIME,
			ZCL_DATATYPE_UINT32,
			ACCESS_CONTROL_READ | ACCESS_CONTROL_WRITE,
			(void *)zclTime
		}
	},
	// *** PDS Cluster Attributes ***
	{
		ZCL_CLUSTER_ID_MS_OCCUPANCY_SENSING,
		{ // Attribute record
			ATTRID_WISYS_MS_PDS_MEASURED_VALUE,
			ZCL_DATATYPE_UINT16,
			ACCESS_CONTROL_READ,
			(void *)zclPdsData
		}
	},

	// *** Battery Cluster Attributes ***
	{
		ZCL_CLUSTER_ID_GEN_POWER_CFG,
		{ // Attribute record
			ATTRID_WISYS_POWER_CFG_BATTERY_PERCENTAGE_REMAINING,
			ZCL_DATATYPE_UINT16,
			ACCESS_CONTROL_READ,
			(void *)zclpdsbatdata
		}
	},

};
#define ZCL_MAX_PDS_INCLUSTERS       2
const cId_t zcl_PDS_InClusterList[ZCL_MAX_PDS_INCLUSTERS] =
{	ZCL_CLUSTER_ID_WISYS_CLUSTER,
	ZCL_CLUSTER_ID_GEN_TIME
};

#define ZCL_MAX_PDS_OUTCLUSTERS       4
const cId_t zcl_PDS_OutClusterList[ZCL_MAX_PDS_OUTCLUSTERS] =
{	ZCL_CLUSTER_ID_WISYS_CLUSTER,
	ZCL_CLUSTER_ID_GEN_TIME,
	ZCL_CLUSTER_ID_MS_OCCUPANCY_SENSING,
	ZCL_CLUSTER_ID_GEN_POWER_CFG
};

SimpleDescriptionFormat_t zcl_PdsDesc =
{	PDS_ENDPOINT,                  //  int Endpoint;
	ZCL_HA_PROFILE_ID,                     //  uint16 AppProfId[2];
	ZCL_HA_DEVICEID_OCCUPANCY_SENSOR,        //  uint16 AppDeviceId[2];
	SAMPLELIGHT_DEVICE_VERSION,            //  int   AppDevVer:4;
	SAMPLELIGHT_FLAGS,                     //  int   AppFlags:4;
	ZCL_MAX_PDS_INCLUSTERS,         //  byte  AppNumInClusters;
	(cId_t *)zcl_PDS_InClusterList, //  byte *pAppInClusterList;
	ZCL_MAX_PDS_OUTCLUSTERS,        //  byte  AppNumInClusters;
	(cId_t *)zcl_PDS_OutClusterList //  byte *pAppInClusterList;
};
#endif

#if defined (HAL_WM)
CONST zclAttrRec_t zclWm_Attrs[WM_MAX_ATTRIBUTES] =
{	{
		ZCL_CLUSTER_ID_WISYS_CLUSTER,
		{ // Attribute record
			ATTRID_WISYS_MGNT_VALUE,
			ZCL_DATATYPE_UINT40,
			ACCESS_CONTROL_READ,
			(void *)zclCounter_wm
		}
	},
	{
		ZCL_CLUSTER_ID_GEN_TIME,
		{ // Attribute record
			ATTRID_TIME_TIME,
			ZCL_DATATYPE_UINT32,
			ACCESS_CONTROL_READ | ACCESS_CONTROL_WRITE,
			(void *)zclTime
		}
	},
	// *** WM Cluster Attributes ***
	{
		ZCL_CLUSTER_ID_MS_FLOW_MEASUREMENT,
		{ // Attribute record
			ATTRID_WISYS_MS_WM_MEASURED_VALUE,
			ZCL_DATATYPE_UINT16,
			ACCESS_CONTROL_READ,
			(void *)zclWmData
		}
	},

};
#define ZCL_MAX_WM_INCLUSTERS       2
const cId_t zcl_WM_InClusterList[ZCL_MAX_WM_INCLUSTERS] =
{	ZCL_CLUSTER_ID_WISYS_CLUSTER,
	ZCL_CLUSTER_ID_GEN_TIME
};

#define ZCL_MAX_WM_OUTCLUSTERS       3
const cId_t zcl_WM_OutClusterList[ZCL_MAX_WM_OUTCLUSTERS] =
{	ZCL_CLUSTER_ID_WISYS_CLUSTER,
	ZCL_CLUSTER_ID_GEN_TIME,
	ZCL_CLUSTER_ID_MS_FLOW_MEASUREMENT,
};

SimpleDescriptionFormat_t zcl_WmDesc =
{	WM_ENDPOINT,                  //  int Endpoint;
	ZCL_HA_PROFILE_ID,                     //  uint16 AppProfId[2];
	ZCL_HA_DEVICEID_FLOW_SENSOR,        //  uint16 AppDeviceId[2];
	SAMPLELIGHT_DEVICE_VERSION,            //  int   AppDevVer:4;
	SAMPLELIGHT_FLAGS,                     //  int   AppFlags:4;
	ZCL_MAX_WM_INCLUSTERS,         //  byte  AppNumInClusters;
	(cId_t *)zcl_WM_InClusterList, //  byte *pAppInClusterList;
	ZCL_MAX_WM_OUTCLUSTERS,        //  byte  AppNumInClusters;
	(cId_t *)zcl_WM_OutClusterList //  byte *pAppInClusterList;
};
#endif

/*********************************************************************
*********Sensor Sensor***********************
**********************************************************************/

#if defined (HAL_VALVE)

CONST zclAttrRec_t zclValve_Attrs[VALVE_MAX_ATTRIBUTES] =
{	{
		ZCL_CLUSTER_ID_WISYS_CLUSTER,
		{ // Attribute record
			ATTRID_WISYS_MGNT_VALUE,
			ZCL_DATATYPE_UINT40,
			ACCESS_CONTROL_READ,
			(void *)zclCounter_valve
		}
	},
	{
		ZCL_CLUSTER_ID_GEN_TIME,
		{ // Attribute record
			ATTRID_TIME_TIME,
			ZCL_DATATYPE_UINT32,
			ACCESS_CONTROL_READ | ACCESS_CONTROL_WRITE,
			(void *)zclTime
		}
	},
	// *** DIMMER Cluster Attributes ***
	{
		ZCL_CLUSTER_ID_GEN_ANALOG_INPUT_BASIC,
		{ // Attribute record
			ATTRID_WISYS_PWM_BASIC_PRESENT_VALUE,
			ZCL_DATATYPE_UINT16,
			ACCESS_CONTROL_READ,
			(void *)zclValveData
		}
	},
};

#define ZCL_MAX_DIMMER_OUTCLUSTERS       3
#define ZCL_MAX_DIMMER_INCLUSTERS       3
const cId_t zcl_DIMMER_InClusterList[ZCL_MAX_DIMMER_INCLUSTERS] =
{	ZCL_CLUSTER_ID_WISYS_CLUSTER,
	ZCL_CLUSTER_ID_GEN_LEVEL_CONTROL,
	ZCL_CLUSTER_ID_GEN_TIME
};
const cId_t zcl_DIMMER_OutClusterList[ZCL_MAX_DIMMER_OUTCLUSTERS] =
{	ZCL_CLUSTER_ID_WISYS_CLUSTER,
	ZCL_CLUSTER_ID_GEN_TIME,
	ZCL_CLUSTER_ID_GEN_ANALOG_INPUT_BASIC,
};
SimpleDescriptionFormat_t zcl_ValveDesc =
{	VALVE_ENDPOINT,                  //  int Endpoint;
	ZCL_HA_PROFILE_ID,                     //  uint16 AppProfId[2];
	ZCL_HA_DEVICEID_DIMMABLE_LIGHT,        //  uint16 AppDeviceId[2];
	SAMPLELIGHT_DEVICE_VERSION,            //  int   AppDevVer:4;
	SAMPLELIGHT_FLAGS,                     //  int   AppFlags:4;
	ZCL_MAX_DIMMER_INCLUSTERS,         //  byte  AppNumInClusters;
	(cId_t *)zcl_DIMMER_InClusterList, //  byte *pAppInClusterList;
	ZCL_MAX_DIMMER_OUTCLUSTERS,        //  byte  AppNumInClusters;
	(cId_t *)zcl_DIMMER_OutClusterList //  byte *pAppInClusterList;
};
#endif

/*********************************************************************
*********HT Sensor***********************
**********************************************************************/

#if defined (HAL_DATA_LOGGER_CHINA) || defined (HAL_DATA_LOGGER_8051)
CONST zclAttrRec_t zclHt_Attrs[HT_MAX_ATTRIBUTES] =
{	{
		ZCL_CLUSTER_ID_WISYS_CLUSTER,
		{ // Attribute record
			ATTRID_WISYS_MGNT_VALUE,
			ZCL_DATATYPE_UINT40,
			ACCESS_CONTROL_READ,
			(void *)zclCounter_data_logger
		}
	},
	{
		ZCL_CLUSTER_ID_GEN_TIME,
		{ // Attribute record
			ATTRID_TIME_TIME,
			ZCL_DATATYPE_UINT32,
			ACCESS_CONTROL_READ | ACCESS_CONTROL_WRITE,
			(void *)zclTime
		}
	},
	// *** Analog Cluster Attributes ***
	{
		ZCL_CLUSTER_ID_MS_RELATIVE_HUMIDITY,
		{ // Attribute record
			ATTRID_WISYS_MS_RELATIVE_HUMIDITY_MEASURED_VALUE,
			ZCL_DATATYPE_UINT16,
			ACCESS_CONTROL_READ,
			(void *)zclHumidData
		}
	},

	// *** Analog Cluster Attributes ***
	{
		ZCL_CLUSTER_ID_MS_TEMPERATURE_MEASUREMENT,
		{ // Attribute record
			ATTRID_WISYS_MS_TEMPERATURE_MEASURED_VALUE,
			ZCL_DATATYPE_UINT16,
			ACCESS_CONTROL_READ,
			(void *)zclTempData
		}
	},

	// *** Battery Cluster Attributes ***
	{
		ZCL_CLUSTER_ID_GEN_POWER_CFG,
		{ // Attribute record
			ATTRID_WISYS_POWER_CFG_BATTERY_PERCENTAGE_REMAINING,
			ZCL_DATATYPE_UINT16,
			ACCESS_CONTROL_READ,
			(void *)zclhtbatdata
		}
	},

};

#define ZCL_MAX_HT_INCLUSTERS       2
const cId_t zcl_HT_InClusterList[ZCL_MAX_HT_INCLUSTERS] =
{	ZCL_CLUSTER_ID_WISYS_CLUSTER,
	ZCL_CLUSTER_ID_GEN_TIME
};

#define ZCL_MAX_HT_OUTCLUSTERS       5
const cId_t zcl_HT_OutClusterList[ZCL_MAX_HT_OUTCLUSTERS] =
{	ZCL_CLUSTER_ID_WISYS_CLUSTER,
	ZCL_CLUSTER_ID_MS_RELATIVE_HUMIDITY,
	ZCL_CLUSTER_ID_MS_TEMPERATURE_MEASUREMENT,
	ZCL_CLUSTER_ID_GEN_TIME,	
	ZCL_CLUSTER_ID_GEN_POWER_CFG
};

SimpleDescriptionFormat_t zcl_HtDesc =
{	HT_ENDPOINT,                  //  int Endpoint;
	ZCL_HA_PROFILE_ID,                     //  uint16 AppProfId[2];
	ZCL_HA_DEVICEID_DIMMABLE_LIGHT,        //  uint16 AppDeviceId[2];
	SAMPLELIGHT_DEVICE_VERSION,            //  int   AppDevVer:4;
	SAMPLELIGHT_FLAGS,                     //  int   AppFlags:4;
	ZCL_MAX_HT_INCLUSTERS,         //  byte  AppNumInClusters;
	(cId_t *)zcl_HT_InClusterList, //  byte *pAppInClusterList;
	ZCL_MAX_HT_OUTCLUSTERS,        //  byte  AppNumInClusters;
	(cId_t *)zcl_HT_OutClusterList //  byte *pAppInClusterList;
};

#endif

/*********************************************************************
* GLOBAL FUNCTIONS
*/

/*********************************************************************
* LOCAL FUNCTIONS
*/

/****************************************************************************
****************************************************************************/

