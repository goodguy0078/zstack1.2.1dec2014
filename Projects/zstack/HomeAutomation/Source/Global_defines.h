//#define DIMMER 
//#define HUMID_DATA
#define DEFAULT_REPORT_INTERVAL 60000

#if defined (HAL_PYROTECH_OFFICE)
    #define HAL_DIMMER
    #define HAL_PDS_EXT
    #define HAL_DIMMER_PYROTECH
    #define ZCL_EZMODE
    #define ZCL_IDENTIFY

#elif defined (HAL_PYROTECH_STREETLIGHT)
    #define HAL_DIMMER
    #define HAL_DIMMER_PYROTECH
    #define HAL_RTC

#elif defined (HAL_AVNI_STREETLIGHT)
    #define HAL_DIMMER
    #define HAL_DIMMER_AVNI
    #define HAL_RTC
    #define HAL_DIMMER_ADC

#else
    #error Please define vendor
#endif
